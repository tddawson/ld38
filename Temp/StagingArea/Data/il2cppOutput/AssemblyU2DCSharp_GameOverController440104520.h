﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOverController
struct  GameOverController_t440104520  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text GameOverController::dateText
	Text_t356221433 * ___dateText_2;
	// UnityEngine.UI.Text GameOverController::obituaryText
	Text_t356221433 * ___obituaryText_3;

public:
	inline static int32_t get_offset_of_dateText_2() { return static_cast<int32_t>(offsetof(GameOverController_t440104520, ___dateText_2)); }
	inline Text_t356221433 * get_dateText_2() const { return ___dateText_2; }
	inline Text_t356221433 ** get_address_of_dateText_2() { return &___dateText_2; }
	inline void set_dateText_2(Text_t356221433 * value)
	{
		___dateText_2 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_2, value);
	}

	inline static int32_t get_offset_of_obituaryText_3() { return static_cast<int32_t>(offsetof(GameOverController_t440104520, ___obituaryText_3)); }
	inline Text_t356221433 * get_obituaryText_3() const { return ___obituaryText_3; }
	inline Text_t356221433 ** get_address_of_obituaryText_3() { return &___obituaryText_3; }
	inline void set_obituaryText_3(Text_t356221433 * value)
	{
		___obituaryText_3 = value;
		Il2CppCodeGenWriteBarrier(&___obituaryText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
