﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CharacterManager2152395578.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "AssemblyU2DCSharp_Conocidos996939829.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Outfit3147864971.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2516986103.h"
#include "AssemblyU2DCSharp_GameOverController440104520.h"
#include "AssemblyU2DCSharp_SocialLog3634626921.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3434760161.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2969489835.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_SocialLogEntry4065639029.h"
#include "AssemblyU2DCSharp_HighScoreController440238002.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_LayerManager147085860.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "AssemblyU2DCSharp_NotificationController1306267903.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_PersonController256544891.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_ScoreManager3573108141.h"
#include "AssemblyU2DCSharp_Relationship3804803770.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_PicturePersonController1772711123.h"
#include "AssemblyU2DCSharp_SpawnPeople2291462312.h"
#include "AssemblyU2DCSharp_PlayerController4148409433.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule2888832346.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "AssemblyU2DCSharp_RungManager3773876531.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2051715777.h"
#include "AssemblyU2DCSharp_YearbookController2017796348.h"

// CharacterManager
struct CharacterManager_t2152395578;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Conocidos
struct Conocidos_t996939829;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Outfit
struct Outfit_t3147864971;
// System.Collections.Generic.List`1<Outfit>
struct List_1_t2516986103;
// GameOverController
struct GameOverController_t440104520;
// System.Collections.Generic.List`1<SocialLogEntry>
struct List_1_t3434760161;
// System.String
struct String_t;
// SocialLogEntry
struct SocialLogEntry_t4065639029;
// HighScoreController
struct HighScoreController_t440238002;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// LayerManager
struct LayerManager_t147085860;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Renderer
struct Renderer_t257310565;
// NotificationController
struct NotificationController_t1306267903;
// UnityEngine.Animator
struct Animator_t69676727;
// PersonController
struct PersonController_t256544891;
// ScoreManager
struct ScoreManager_t3573108141;
// UnityEngine.Object
struct Object_t1021602117;
// PicturePersonController
struct PicturePersonController_t1772711123;
// SpawnPeople
struct SpawnPeople_t2291462312;
// PlayerController
struct PlayerController_t4148409433;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// RungManager
struct RungManager_t3773876531;
// UnityEngine.UI.Image
struct Image_t2042527209;
// SocialLog
struct SocialLog_t3634626921;
// YearbookController
struct YearbookController_t2017796348;
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterManager_t2152395578_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t CharacterManager__cctor_m1220825452_MetadataUsageId;
extern Il2CppClass* Conocidos_t996939829_il2cpp_TypeInfo_var;
extern Il2CppClass* Outfit_t3147864971_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m349643092_MethodInfo_var;
extern const uint32_t Conocidos_Add_m3740580729_MetadataUsageId;
extern Il2CppClass* List_1_t2516986103_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2219254136_MethodInfo_var;
extern const uint32_t Conocidos__cctor_m1730778583_MetadataUsageId;
extern Il2CppClass* SocialLog_t3634626921_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m4030601119_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1612170365_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m3863334719_MethodInfo_var;
extern const uint32_t GameOverController_Start_m369088627_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2993481669;
extern const uint32_t GameOverController_Update_m2787187014_MetadataUsageId;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3364115282;
extern const uint32_t GameOverController_UpdateDate_m2907151458_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3347186379_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2300213839_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m398828707_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1345971145_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2892548871;
extern Il2CppCodeGenString* _stringLiteral2642836381;
extern Il2CppCodeGenString* _stringLiteral868350994;
extern Il2CppCodeGenString* _stringLiteral3636943112;
extern Il2CppCodeGenString* _stringLiteral2369751460;
extern Il2CppCodeGenString* _stringLiteral2636872654;
extern Il2CppCodeGenString* _stringLiteral1505878660;
extern Il2CppCodeGenString* _stringLiteral2162321594;
extern Il2CppCodeGenString* _stringLiteral1671415501;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t GameOverController_UpdateObituary_m263256801_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3532113296;
extern Il2CppCodeGenString* _stringLiteral2541884565;
extern Il2CppCodeGenString* _stringLiteral573557715;
extern Il2CppCodeGenString* _stringLiteral1237663345;
extern Il2CppCodeGenString* _stringLiteral1306849384;
extern const uint32_t HighScoreController_Start_m205779257_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t3275118058_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t3275118058_m235623703_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral3387963429;
extern Il2CppCodeGenString* _stringLiteral69877657;
extern const uint32_t LayerManager_SetLayer_m2846485351_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3029058044;
extern Il2CppCodeGenString* _stringLiteral3819852093;
extern const uint32_t NotificationController_ShowNotification_m3734402192_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1903744674;
extern Il2CppCodeGenString* _stringLiteral2622853536;
extern Il2CppCodeGenString* _stringLiteral1604514370;
extern const uint32_t Outfit__ctor_m80864268_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral391991650;
extern Il2CppCodeGenString* _stringLiteral552427262;
extern const uint32_t PersonController_Start_m816453704_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m2287078133_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2415992412;
extern Il2CppCodeGenString* _stringLiteral1275666007;
extern Il2CppCodeGenString* _stringLiteral3815830359;
extern Il2CppCodeGenString* _stringLiteral2054002869;
extern const uint32_t PersonController_Update_m1728911355_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1005515892;
extern Il2CppCodeGenString* _stringLiteral2064390248;
extern Il2CppCodeGenString* _stringLiteral2105580454;
extern const uint32_t PersonController_RandomizeAppearance_m3637759145_MetadataUsageId;
extern const uint32_t PersonController_DressIn_m2898277795_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2546340657;
extern Il2CppCodeGenString* _stringLiteral3869745644;
extern const uint32_t PersonController_MakeAcquaintance_m2362241201_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1077196766;
extern Il2CppCodeGenString* _stringLiteral3517285394;
extern const uint32_t PersonController_MakeFriend_m3389549496_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3291630071;
extern const uint32_t PersonController_MakeBFF_m2790855184_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1465256952;
extern Il2CppCodeGenString* _stringLiteral2949419224;
extern const uint32_t PersonController_MakeBully_m3193116814_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2962743445;
extern const uint32_t PersonController_StartWalking_m2205466975_MetadataUsageId;
extern const uint32_t PersonController_StopWalking_m2084113711_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2237410460;
extern const uint32_t PersonController_Wave_m902357639_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral851542544;
extern const uint32_t PersonController_FistBump_m3979884468_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1751001121;
extern const uint32_t PersonController_Hug_m4220064788_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2898688624;
extern const uint32_t PersonController_Kick_m2572509440_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1329086068;
extern const uint32_t PersonController_PauseWalking_m2168070195_MetadataUsageId;
extern const uint32_t PicturePersonController_Start_m2577446190_MetadataUsageId;
extern const uint32_t PicturePersonController_RandomizeAppearance_m1140662613_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral152376095;
extern const uint32_t PicturePersonController_AddDuringGame_m973809354_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1299152942;
extern const uint32_t PlayerController_Start_m3606284888_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern const uint32_t PlayerController_HandleMovementInput_m3771922523_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1884423134;
extern const uint32_t PlayerController_HandleInteractionInput_m2604908464_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3178038520;
extern Il2CppCodeGenString* _stringLiteral87538262;
extern Il2CppCodeGenString* _stringLiteral924979642;
extern Il2CppCodeGenString* _stringLiteral2208644768;
extern Il2CppCodeGenString* _stringLiteral2719378512;
extern const uint32_t PlayerController_Wave_m2332726893_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2271763354;
extern Il2CppCodeGenString* _stringLiteral3628357200;
extern Il2CppCodeGenString* _stringLiteral2910639801;
extern Il2CppCodeGenString* _stringLiteral959716072;
extern Il2CppCodeGenString* _stringLiteral622842013;
extern Il2CppCodeGenString* _stringLiteral4159693655;
extern const uint32_t PlayerController_FistBump_m3512763140_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1028204982;
extern Il2CppCodeGenString* _stringLiteral3074348318;
extern Il2CppCodeGenString* _stringLiteral2821890191;
extern Il2CppCodeGenString* _stringLiteral2273171932;
extern Il2CppCodeGenString* _stringLiteral2267285084;
extern const uint32_t PlayerController_Hug_m1326409484_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749_MethodInfo_var;
extern const uint32_t PlayerController_UpdateEyeContact_m3487024180_MetadataUsageId;
extern const uint32_t PlayerController_CanSee_m1619983851_MetadataUsageId;
extern const uint32_t RungManager_Start_m3245805540_MetadataUsageId;
extern const uint32_t RungManager_ClimbLadder_m4020101199_MetadataUsageId;
extern const uint32_t ScoreManager_Start_m1676398040_MetadataUsageId;
extern const uint32_t ScoreManager_Penalize_m3478917589_MetadataUsageId;
extern const uint32_t ScoreManager_Award_m2374896584_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const uint32_t ScoreManager_UpdateGUI_m653588760_MetadataUsageId;
extern Il2CppClass* SocialLogEntry_t4065639029_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m608678902_MethodInfo_var;
extern const uint32_t SocialLog_Add_m2722617324_MetadataUsageId;
extern Il2CppClass* List_1_t3434760161_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1518945482_MethodInfo_var;
extern const uint32_t SocialLog__cctor_m527127387_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral304499897;
extern Il2CppCodeGenString* _stringLiteral3636943121;
extern Il2CppCodeGenString* _stringLiteral3697230210;
extern const uint32_t SocialLogEntry_ToString_m3714031703_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3034765985;
extern const uint32_t SpawnPeople_Start_m1455043971_MetadataUsageId;
extern const MethodInfo* List_1_GetEnumerator_m998891817_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4263520361_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2115421005_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1278152915_MethodInfo_var;
extern const uint32_t SpawnPeople_SpawnAcquaintances_m1057452436_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2371106403;
extern Il2CppCodeGenString* _stringLiteral1977266746;
extern Il2CppCodeGenString* _stringLiteral3335134194;
extern const uint32_t SpawnPeople_AddFromOutfit_m1509951326_MetadataUsageId;
extern const uint32_t SpawnPeople_SpawnStranger_m1484849674_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3538073363;
extern const uint32_t YearbookController_Update_m3130861606_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m681991875_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Transform_t3275118058 * p1, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m704351054(__this, method) ((  void (*) (List_1_t1125654279 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Outfit::.ctor(UnityEngine.GameObject)
extern "C"  void Outfit__ctor_m80864268 (Outfit_t3147864971 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Outfit>::Add(!0)
#define List_1_Add_m349643092(__this, p0, method) ((  void (*) (List_1_t2516986103 *, Outfit_t3147864971 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<Outfit>::.ctor()
#define List_1__ctor_m2219254136(__this, method) ((  void (*) (List_1_t2516986103 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void GameOverController::UpdateDate()
extern "C"  void GameOverController_UpdateDate_m2907151458 (GameOverController_t440104520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameOverController::UpdateObituary()
extern "C"  void GameOverController_UpdateObituary_m263256801 (GameOverController_t440104520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Clear()
#define List_1_Clear_m4030601119(__this, method) ((  void (*) (List_1_t1125654279 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Outfit>::Clear()
#define List_1_Clear_m1612170365(__this, method) ((  void (*) (List_1_t2516986103 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SocialLogEntry>::Clear()
#define List_1_Clear_m3863334719(__this, method) ((  void (*) (List_1_t3434760161 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1771960377 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::get_Now()
extern "C"  DateTime_t693205669  DateTime_get_Now_m24136300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString(System.String)
extern "C"  String_t* DateTime_ToString_m1473013667 (DateTime_t693205669 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m2889062785 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1561703559 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<SocialLogEntry>::GetEnumerator()
#define List_1_GetEnumerator_m3347186379(__this, method) ((  Enumerator_t2969489835  (*) (List_1_t3434760161 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<SocialLogEntry>::get_Current()
#define Enumerator_get_Current_m2300213839(__this, method) ((  SocialLogEntry_t4065639029 * (*) (Enumerator_t2969489835 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.String SocialLogEntry::ToString()
extern "C"  String_t* SocialLogEntry_ToString_m3714031703 (SocialLogEntry_t4065639029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<SocialLogEntry>::MoveNext()
#define Enumerator_MoveNext_m398828707(__this, method) ((  bool (*) (Enumerator_t2969489835 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SocialLogEntry>::Dispose()
#define Enumerator_Dispose_m1345971145(__this, method) ((  void (*) (Enumerator_t2969489835 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m1506372053 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m2712461877 (GameObject_t1756533147 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3275118058 * Transform_Find_m3323476454 (Transform_t3275118058 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t3275118058_m235623703(__this, method) ((  Transform_t3275118058 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
extern "C"  void Renderer_set_sortingLayerName_m3726155509 (Renderer_t257310565 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m3418492570 (Animator_t69676727 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C"  Transform_t3275118058 * Transform_FindChild_m2677714886 (Transform_t3275118058 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C"  Color_t2020392075  SpriteRenderer_get_color_m345525162 (SpriteRenderer_t1209076198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PersonController::RandomizeXPos()
extern "C"  float PersonController_RandomizeXPos_m1090418429 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PersonController::RandomizeYPos()
extern "C"  float PersonController_RandomizeYPos_m1453813666 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<ScoreManager>()
#define GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996(__this, method) ((  ScoreManager_t3573108141 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void PersonController::StartWalking()
extern "C"  void PersonController_StartWalking_m2205466975 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::RandomizeAppearance()
extern "C"  void PersonController_RandomizeAppearance_m3637759145 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m3441471442(__this, p0, method) ((  void (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t2243707579  Vector2_get_right_m28012078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_op_Multiply_m4236139442 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m3316827744 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PersonController::WalkedOffScreen()
extern "C"  bool PersonController_WalkedOffScreen_m2012382181 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(!0)
#define List_1_Remove_m2287078133(__this, p0, method) ((  bool (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void SocialLog::Add(System.Int32,System.String)
extern "C"  void SocialLog_Add_m2722617324 (Il2CppObject * __this /* static, unused */, int32_t ___points0, String_t* ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager::Penalize(System.Int32)
extern "C"  void ScoreManager_Penalize_m3478917589 (ScoreManager_t3573108141 * __this, int32_t ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::StopWalking()
extern "C"  void PersonController_StopWalking_m2084113711 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::TurnAround()
extern "C"  void PersonController_TurnAround_m2621683120 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerManager::SetLayer(UnityEngine.GameObject,System.Int32)
extern "C"  void LayerManager_SetLayer_m2846485351 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___person0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Outfit::.ctor()
extern "C"  void Outfit__ctor_m2350389386 (Outfit_t3147864971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m2339931967 (SpriteRenderer_t1209076198 * __this, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<NotificationController>()
#define GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(__this, method) ((  NotificationController_t1306267903 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void NotificationController::ShowNotification(System.String)
extern "C"  void NotificationController_ShowNotification_m3734402192 (NotificationController_t1306267903 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m2650940523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m2305662531 (Animator_t69676727 * __this, String_t* p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::PauseWalking(System.Single,System.Boolean)
extern "C"  void PersonController_PauseWalking_m2168070195 (PersonController_t256544891 * __this, float ___seconds0, bool ___runAway1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C"  void Renderer_set_sortingOrder_m809829562 (Renderer_t257310565 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PicturePersonController::RandomizeAppearance()
extern "C"  void PicturePersonController_RandomizeAppearance_m1140662613 (PicturePersonController_t1772711123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Conocidos::Add(UnityEngine.GameObject)
extern "C"  void Conocidos_Add_m3740580729 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___person0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnPeople::AddFromOutfit(Outfit)
extern "C"  void SpawnPeople_AddFromOutfit_m1509951326 (SpawnPeople_t2291462312 * __this, Outfit_t3147864971 * ___outfit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::HandleMovementInput()
extern "C"  void PlayerController_HandleMovementInput_m3771922523 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::HandleInteractionInput()
extern "C"  void PlayerController_HandleInteractionInput_m2604908464 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::UpdateEyeContact()
extern "C"  void PlayerController_UpdateEyeContact_m3487024180 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m2098048324 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController::CanMoveUp()
extern "C"  bool PlayerController_CanMoveUp_m4049947684 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController::CanMoveDown()
extern "C"  bool PlayerController_CanMoveDown_m345328991 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Wave()
extern "C"  void PlayerController_Wave_m2332726893 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::FistBump()
extern "C"  void PlayerController_FistBump_m3512763140 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Hug()
extern "C"  void PlayerController_Hug_m1326409484 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PersonController>()
#define GameObject_GetComponent_TisPersonController_t256544891_m234165472(__this, method) ((  PersonController_t256544891 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void ScoreManager::Award(System.Int32)
extern "C"  void ScoreManager_Award_m2374896584 (ScoreManager_t3573108141 * __this, int32_t ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::Wave()
extern "C"  void PersonController_Wave_m902357639 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::MakeFriend()
extern "C"  void PersonController_MakeFriend_m3389549496 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::MakeAcquaintance()
extern "C"  void PersonController_MakeAcquaintance_m2362241201 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::FistBump()
extern "C"  void PersonController_FistBump_m3979884468 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::MakeBFF()
extern "C"  void PersonController_MakeBFF_m2790855184 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::Hug()
extern "C"  void PersonController_Hug_m4220064788 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersonController::Kick()
extern "C"  void PersonController_Kick_m2572509440 (PersonController_t256544891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController::CanSee(UnityEngine.GameObject)
extern "C"  bool PlayerController_CanSee_m1619983851 (PlayerController_t4148409433 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
#define List_1_GetEnumerator_m970439620(__this, method) ((  Enumerator_t660383953  (*) (List_1_t1125654279 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m2389888842(__this, method) ((  GameObject_t1756533147 * (*) (Enumerator_t660383953 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m3635305532(__this, method) ((  bool (*) (Enumerator_t660383953 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m1242097970(__this, method) ((  void (*) (Enumerator_t660383953 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749(__this, method) ((  ParticleSystem_t3394631041 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern "C"  ShapeModule_t2888832346  ParticleSystem_get_shape_m1960653537 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m3074381503 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_box(UnityEngine.Vector3)
extern "C"  void ShapeModule_set_box_m3050412983 (ShapeModule_t2888832346 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RungManager::UpdateGUI()
extern "C"  void RungManager_UpdateGUI_m4113894772 (RungManager_t3773876531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager::UpdateGUI()
extern "C"  void ScoreManager_UpdateGUI_m653588760 (ScoreManager_t3573108141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager::LevelUp()
extern "C"  void ScoreManager_LevelUp_m2694938423 (ScoreManager_t3573108141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1648492575 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RungManager::ClimbLadder()
extern "C"  int32_t RungManager_ClimbLadder_m4020101199 (RungManager_t3773876531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PicturePersonController::AddDuringGame()
extern "C"  void PicturePersonController_AddDuringGame_m973809354 (PicturePersonController_t1772711123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocialLogEntry::.ctor(System.Int32,System.String)
extern "C"  void SocialLogEntry__ctor_m2511064239 (SocialLogEntry_t4065639029 * __this, int32_t ___points0, String_t* ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<SocialLogEntry>::Add(!0)
#define List_1_Add_m608678902(__this, p0, method) ((  void (*) (List_1_t3434760161 *, SocialLogEntry_t4065639029 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<SocialLogEntry>::.ctor()
#define List_1__ctor_m1518945482(__this, method) ((  void (*) (List_1_t3434760161 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void SpawnPeople::SpawnAcquaintances()
extern "C"  void SpawnPeople_SpawnAcquaintances_m1057452436 (SpawnPeople_t2291462312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Outfit>::GetEnumerator()
#define List_1_GetEnumerator_m998891817(__this, method) ((  Enumerator_t2051715777  (*) (List_1_t2516986103 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Outfit>::get_Current()
#define Enumerator_get_Current_m4263520361(__this, method) ((  Outfit_t3147864971 * (*) (Enumerator_t2051715777 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Outfit>::MoveNext()
#define Enumerator_MoveNext_m2115421005(__this, method) ((  bool (*) (Enumerator_t2051715777 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Outfit>::Dispose()
#define Enumerator_Dispose_m1278152915(__this, method) ((  void (*) (Enumerator_t2051715777 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m3066053529(__this /* static, unused */, p0, p1, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.GameObject::SendMessage(System.String)
extern "C"  void GameObject_SendMessage_m1177535567 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object)
extern "C"  void GameObject_SendMessage_m2115020133 (GameObject_t1756533147 * __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterManager::.ctor()
extern "C"  void CharacterManager__ctor_m3909797101 (CharacterManager_t2152395578 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterManager::.cctor()
extern "C"  void CharacterManager__cctor_m1220825452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterManager__cctor_m1220825452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		((CharacterManager_t2152395578_StaticFields*)CharacterManager_t2152395578_il2cpp_TypeInfo_var->static_fields)->set_characters_2(L_0);
		return;
	}
}
// System.Void Conocidos::.ctor()
extern "C"  void Conocidos__ctor_m422587292 (Conocidos_t996939829 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Conocidos::Add(UnityEngine.GameObject)
extern "C"  void Conocidos_Add_m3740580729 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___person0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Conocidos_Add_m3740580729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Conocidos_t996939829_il2cpp_TypeInfo_var);
		List_1_t2516986103 * L_0 = ((Conocidos_t996939829_StaticFields*)Conocidos_t996939829_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		GameObject_t1756533147 * L_1 = ___person0;
		Outfit_t3147864971 * L_2 = (Outfit_t3147864971 *)il2cpp_codegen_object_new(Outfit_t3147864971_il2cpp_TypeInfo_var);
		Outfit__ctor_m80864268(L_2, L_1, /*hidden argument*/NULL);
		List_1_Add_m349643092(L_0, L_2, /*hidden argument*/List_1_Add_m349643092_MethodInfo_var);
		return;
	}
}
// System.Void Conocidos::.cctor()
extern "C"  void Conocidos__cctor_m1730778583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Conocidos__cctor_m1730778583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2516986103 * L_0 = (List_1_t2516986103 *)il2cpp_codegen_object_new(List_1_t2516986103_il2cpp_TypeInfo_var);
		List_1__ctor_m2219254136(L_0, /*hidden argument*/List_1__ctor_m2219254136_MethodInfo_var);
		((Conocidos_t996939829_StaticFields*)Conocidos_t996939829_il2cpp_TypeInfo_var->static_fields)->set_list_0(L_0);
		return;
	}
}
// System.Void GameOverController::.ctor()
extern "C"  void GameOverController__ctor_m4085781695 (GameOverController_t440104520 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverController::Start()
extern "C"  void GameOverController_Start_m369088627 (GameOverController_t440104520 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverController_Start_m369088627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameOverController_UpdateDate_m2907151458(__this, /*hidden argument*/NULL);
		GameOverController_UpdateObituary_m263256801(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterManager_t2152395578_il2cpp_TypeInfo_var);
		List_1_t1125654279 * L_0 = ((CharacterManager_t2152395578_StaticFields*)CharacterManager_t2152395578_il2cpp_TypeInfo_var->static_fields)->get_characters_2();
		List_1_Clear_m4030601119(L_0, /*hidden argument*/List_1_Clear_m4030601119_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Conocidos_t996939829_il2cpp_TypeInfo_var);
		List_1_t2516986103 * L_1 = ((Conocidos_t996939829_StaticFields*)Conocidos_t996939829_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		List_1_Clear_m1612170365(L_1, /*hidden argument*/List_1_Clear_m1612170365_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		List_1_t3434760161 * L_2 = ((SocialLog_t3634626921_StaticFields*)SocialLog_t3634626921_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		List_1_Clear_m3863334719(L_2, /*hidden argument*/List_1_Clear_m3863334719_MethodInfo_var);
		return;
	}
}
// System.Void GameOverController::Update()
extern "C"  void GameOverController_Update_m2787187014 (GameOverController_t440104520 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverController_Update_m2787187014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)122), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2993481669, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void GameOverController::UpdateDate()
extern "C"  void GameOverController_UpdateDate_m2907151458 (GameOverController_t440104520 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverController_UpdateDate_m2907151458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Text_t356221433 * L_0 = __this->get_dateText_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = DateTime_ToString_m1473013667((&V_0), _stringLiteral3364115282, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void GameOverController::UpdateObituary()
extern "C"  void GameOverController_UpdateObituary_m263256801 (GameOverController_t440104520 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverController_UpdateObituary_m263256801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	SocialLogEntry_t4065639029 * V_3 = NULL;
	Enumerator_t2969489835  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral2892548871;
		String_t* L_0 = V_0;
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral868350994, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1561703559(NULL /*static, unused*/, L_0, _stringLiteral2642836381, L_2, _stringLiteral3636943112, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, _stringLiteral2369751460, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2636872654, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
		String_t* L_8 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m1561703559(NULL /*static, unused*/, L_6, _stringLiteral2642836381, L_8, _stringLiteral3636943112, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, L_10, _stringLiteral1505878660, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = V_0;
		String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, L_12, _stringLiteral2162321594, /*hidden argument*/NULL);
		V_0 = L_13;
		String_t* L_14 = V_0;
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, L_14, _stringLiteral1671415501, /*hidden argument*/NULL);
		V_0 = L_15;
		String_t* L_16 = V_0;
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, L_16, _stringLiteral2162321594, /*hidden argument*/NULL);
		V_0 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		List_1_t3434760161 * L_18 = ((SocialLog_t3634626921_StaticFields*)SocialLog_t3634626921_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		Enumerator_t2969489835  L_19 = List_1_GetEnumerator_m3347186379(L_18, /*hidden argument*/List_1_GetEnumerator_m3347186379_MethodInfo_var);
		V_4 = L_19;
	}

IL_00a2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c1;
		}

IL_00a7:
		{
			SocialLogEntry_t4065639029 * L_20 = Enumerator_get_Current_m2300213839((&V_4), /*hidden argument*/Enumerator_get_Current_m2300213839_MethodInfo_var);
			V_3 = L_20;
			String_t* L_21 = V_0;
			SocialLogEntry_t4065639029 * L_22 = V_3;
			String_t* L_23 = SocialLogEntry_ToString_m3714031703(L_22, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m612901809(NULL /*static, unused*/, L_21, L_23, _stringLiteral372029352, /*hidden argument*/NULL);
			V_0 = L_24;
		}

IL_00c1:
		{
			bool L_25 = Enumerator_MoveNext_m398828707((&V_4), /*hidden argument*/Enumerator_MoveNext_m398828707_MethodInfo_var);
			if (L_25)
			{
				goto IL_00a7;
			}
		}

IL_00cd:
		{
			IL2CPP_LEAVE(0xE0, FINALLY_00d2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d2;
	}

FINALLY_00d2:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1345971145((&V_4), /*hidden argument*/Enumerator_Dispose_m1345971145_MethodInfo_var);
		IL2CPP_END_FINALLY(210)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(210)
	{
		IL2CPP_JUMP_TBL(0xE0, IL_00e0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e0:
	{
		Text_t356221433 * L_26 = __this->get_obituaryText_3();
		String_t* L_27 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_26, L_27);
		return;
	}
}
// System.Void HighScoreController::.ctor()
extern "C"  void HighScoreController__ctor_m4088929369 (HighScoreController_t440238002 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighScoreController::Start()
extern "C"  void HighScoreController_Start_m205779257 (HighScoreController_t440238002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScoreController_Start_m205779257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Text_t356221433 * L_1 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		V_0 = L_1;
		Text_t356221433 * L_2 = V_0;
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		ArrayElementTypeCheck (L_3, _stringLiteral3532113296);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3532113296);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		int32_t L_5 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral2541884565, 0, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		ArrayElementTypeCheck (L_8, _stringLiteral573557715);
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral573557715);
		ObjectU5BU5D_t3614634134* L_9 = L_8;
		int32_t L_10 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1237663345, 0, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = L_9;
		ArrayElementTypeCheck (L_13, _stringLiteral1306849384);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1306849384);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_14);
		return;
	}
}
// System.Void HighScoreController::Update()
extern "C"  void HighScoreController_Update_m658987988 (HighScoreController_t440238002 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LayerManager::.ctor()
extern "C"  void LayerManager__ctor_m3222746979 (LayerManager_t147085860 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerManager::SetLayer(UnityEngine.GameObject,System.Int32)
extern "C"  void LayerManager_SetLayer_m2846485351 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___person0, int32_t ___layer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LayerManager_SetLayer_m2846485351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Transform_t3275118058 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1756533147 * L_0 = ___person0;
		int32_t L_1 = ___layer1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral372029397, L_3, /*hidden argument*/NULL);
		int32_t L_5 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_set_layer_m2712461877(L_0, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = ___person0;
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = Transform_Find_m3323476454(L_7, _stringLiteral3387963429, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_GetComponent_TisTransform_t3275118058_m235623703(L_8, /*hidden argument*/Component_GetComponent_TisTransform_t3275118058_m235623703_MethodInfo_var);
		V_0 = L_9;
		Transform_t3275118058 * L_10 = V_0;
		Il2CppObject * L_11 = Transform_GetEnumerator_m3479720613(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c1;
		}

IL_003d:
		{
			Il2CppObject * L_12 = V_2;
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_12);
			V_1 = ((Transform_t3275118058 *)CastclassClass(L_13, Transform_t3275118058_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_14 = V_1;
			SpriteRenderer_t1209076198 * L_15 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_14, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
			int32_t L_16 = ___layer1;
			int32_t L_17 = L_16;
			Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_19 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral69877657, L_18, /*hidden argument*/NULL);
			Renderer_set_sortingLayerName_m3726155509(L_15, L_19, /*hidden argument*/NULL);
			Transform_t3275118058 * L_20 = V_1;
			Il2CppObject * L_21 = Transform_GetEnumerator_m3479720613(L_20, /*hidden argument*/NULL);
			V_4 = L_21;
		}

IL_006c:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0099;
			}

IL_0071:
			{
				Il2CppObject * L_22 = V_4;
				Il2CppObject * L_23 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_22);
				V_3 = ((Transform_t3275118058 *)CastclassClass(L_23, Transform_t3275118058_il2cpp_TypeInfo_var));
				Transform_t3275118058 * L_24 = V_3;
				SpriteRenderer_t1209076198 * L_25 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_24, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
				int32_t L_26 = ___layer1;
				int32_t L_27 = L_26;
				Il2CppObject * L_28 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_27);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_29 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral69877657, L_28, /*hidden argument*/NULL);
				Renderer_set_sortingLayerName_m3726155509(L_25, L_29, /*hidden argument*/NULL);
			}

IL_0099:
			{
				Il2CppObject * L_30 = V_4;
				bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_30);
				if (L_31)
				{
					goto IL_0071;
				}
			}

IL_00a5:
			{
				IL2CPP_LEAVE(0xC1, FINALLY_00aa);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00aa;
		}

FINALLY_00aa:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_32 = V_4;
				Il2CppObject * L_33 = ((Il2CppObject *)IsInst(L_32, IDisposable_t2427283555_il2cpp_TypeInfo_var));
				V_5 = L_33;
				if (!L_33)
				{
					goto IL_00c0;
				}
			}

IL_00b9:
			{
				Il2CppObject * L_34 = V_5;
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_34);
			}

IL_00c0:
			{
				IL2CPP_END_FINALLY(170)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(170)
		{
			IL2CPP_JUMP_TBL(0xC1, IL_00c1)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00c1:
		{
			Il2CppObject * L_35 = V_2;
			bool L_36 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_35);
			if (L_36)
			{
				goto IL_003d;
			}
		}

IL_00cc:
		{
			IL2CPP_LEAVE(0xE7, FINALLY_00d1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d1;
	}

FINALLY_00d1:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_37 = V_2;
			Il2CppObject * L_38 = ((Il2CppObject *)IsInst(L_37, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_6 = L_38;
			if (!L_38)
			{
				goto IL_00e6;
			}
		}

IL_00df:
		{
			Il2CppObject * L_39 = V_6;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_39);
		}

IL_00e6:
		{
			IL2CPP_END_FINALLY(209)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(209)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e7:
	{
		return;
	}
}
// System.Void NotificationController::.ctor()
extern "C"  void NotificationController__ctor_m2409512510 (NotificationController_t1306267903 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationController::Start()
extern "C"  void NotificationController_Start_m4227402666 (NotificationController_t1306267903 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NotificationController::Update()
extern "C"  void NotificationController_Update_m3869687143 (NotificationController_t1306267903 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NotificationController::ShowNotification(System.String)
extern "C"  void NotificationController_ShowNotification_m3734402192 (NotificationController_t1306267903 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotificationController_ShowNotification_m3734402192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		Animator_t69676727 * L_0 = __this->get_animator_2();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral3029058044, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_FindChild_m2677714886(L_1, _stringLiteral3819852093, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = Component_GetComponent_TisText_t356221433_m1342661039(L_2, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		V_0 = L_3;
		Text_t356221433 * L_4 = V_0;
		String_t* L_5 = ___msg0;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		return;
	}
}
// System.Void Outfit::.ctor(UnityEngine.GameObject)
extern "C"  void Outfit__ctor_m80864268 (Outfit_t3147864971 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Outfit__ctor_m80864268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = ___obj0;
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral1903744674, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_3 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_2, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_4 = SpriteRenderer_get_color_m345525162(L_3, /*hidden argument*/NULL);
		__this->set_shirtColor_2(L_4);
		GameObject_t1756533147 * L_5 = ___obj0;
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Transform_Find_m3323476454(L_6, _stringLiteral2622853536, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_8 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_7, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_9 = SpriteRenderer_get_color_m345525162(L_8, /*hidden argument*/NULL);
		__this->set_skinColor_0(L_9);
		GameObject_t1756533147 * L_10 = ___obj0;
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Transform_Find_m3323476454(L_11, _stringLiteral1604514370, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_13 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_12, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_14 = SpriteRenderer_get_color_m345525162(L_13, /*hidden argument*/NULL);
		__this->set_pantsColor_1(L_14);
		return;
	}
}
// System.Void Outfit::.ctor()
extern "C"  void Outfit__ctor_m2350389386 (Outfit_t3147864971 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		float L_0 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_1 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m1909920690(&L_3, L_0, L_1, L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_shirtColor_2(L_3);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, L_4, L_5, L_6, (1.0f), /*hidden argument*/NULL);
		__this->set_skinColor_0(L_7);
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_10 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, L_8, L_9, L_10, (1.0f), /*hidden argument*/NULL);
		__this->set_pantsColor_1(L_11);
		return;
	}
}
// System.Void PersonController::.ctor()
extern "C"  void PersonController__ctor_m3667032972 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_9((0.5f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::Start()
extern "C"  void PersonController_Start_m816453704 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_Start_m816453704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_interactedWith_10((bool)0);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = PersonController_RandomizeXPos_m1090418429(__this, /*hidden argument*/NULL);
		float L_2 = PersonController_RandomizeYPos_m1453813666(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(L_0, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral391991650, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_6 = GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996(L_5, /*hidden argument*/GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996_MethodInfo_var);
		__this->set_scoreboard_4(L_6);
		GameObject_t1756533147 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral552427262, /*hidden argument*/NULL);
		__this->set_notifier_5(L_7);
		int32_t L_8 = __this->get_relationship_3();
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		PersonController_StartWalking_m2205466975(__this, /*hidden argument*/NULL);
		PersonController_RandomizeAppearance_m3637759145(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterManager_t2152395578_il2cpp_TypeInfo_var);
		List_1_t1125654279 * L_9 = ((CharacterManager_t2152395578_StaticFields*)CharacterManager_t2152395578_il2cpp_TypeInfo_var->static_fields)->get_characters_2();
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		List_1_Add_m3441471442(L_9, L_10, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		return;
	}
}
// System.Void PersonController::Update()
extern "C"  void PersonController_Update_m1728911355 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_Update_m1728911355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_currentlyWalking_7();
		if (!L_0)
		{
			goto IL_0146;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_get_right_m28012078(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_speed_9();
		Vector2_t2243707579  L_6 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Transform_Translate_m3316827744(L_1, L_7, /*hidden argument*/NULL);
		bool L_8 = PersonController_WalkedOffScreen_m2012382181(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0146;
		}
	}
	{
		int32_t L_9 = __this->get_relationship_3();
		if (L_9)
		{
			goto IL_006c;
		}
	}
	{
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterManager_t2152395578_il2cpp_TypeInfo_var);
		List_1_t1125654279 * L_11 = ((CharacterManager_t2152395578_StaticFields*)CharacterManager_t2152395578_il2cpp_TypeInfo_var->static_fields)->get_characters_2();
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		List_1_Remove_m2287078133(L_11, L_12, /*hidden argument*/List_1_Remove_m2287078133_MethodInfo_var);
		goto IL_0146;
	}

IL_006c:
	{
		bool L_13 = __this->get_interactedWith_10();
		if (L_13)
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_14 = __this->get_relationship_3();
		if ((!(((uint32_t)L_14) == ((uint32_t)3))))
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, (-1), _stringLiteral2415992412, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_15 = __this->get_scoreboard_4();
		ScoreManager_Penalize_m3478917589(L_15, 1, /*hidden argument*/NULL);
		goto IL_00eb;
	}

IL_009f:
	{
		int32_t L_16 = __this->get_relationship_3();
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_00c7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, (-1), _stringLiteral1275666007, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_17 = __this->get_scoreboard_4();
		ScoreManager_Penalize_m3478917589(L_17, 2, /*hidden argument*/NULL);
		goto IL_00eb;
	}

IL_00c7:
	{
		int32_t L_18 = __this->get_relationship_3();
		if ((!(((uint32_t)L_18) == ((uint32_t)5))))
		{
			goto IL_00eb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-3), _stringLiteral3815830359, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_19 = __this->get_scoreboard_4();
		ScoreManager_Penalize_m3478917589(L_19, 3, /*hidden argument*/NULL);
	}

IL_00eb:
	{
		PersonController_StopWalking_m2084113711(__this, /*hidden argument*/NULL);
		__this->set_interactedWith_10((bool)0);
		PersonController_TurnAround_m2621683120(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		float L_23 = (&V_0)->get_x_1();
		float L_24 = PersonController_RandomizeYPos_m1453813666(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m3067419446(&L_25, L_23, L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(L_20, L_26, /*hidden argument*/NULL);
		float L_27 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (5.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral2054002869, L_27, /*hidden argument*/NULL);
	}

IL_0146:
	{
		return;
	}
}
// System.Boolean PersonController::WalkedOffScreen()
extern "C"  bool PersonController_WalkedOffScreen_m2012382181 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		bool L_0 = __this->get_headingRight_8();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		if ((((float)L_3) > ((float)(5.0f))))
		{
			goto IL_0052;
		}
	}

IL_0028:
	{
		bool L_4 = __this->get_headingRight_8();
		if (L_4)
		{
			goto IL_004f;
		}
	}
	{
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_1();
		G_B5_0 = ((((float)L_7) < ((float)(-5.0f)))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		G_B7_0 = G_B5_0;
		goto IL_0053;
	}

IL_0052:
	{
		G_B7_0 = 1;
	}

IL_0053:
	{
		return (bool)G_B7_0;
	}
}
// System.Single PersonController::RandomizeYPos()
extern "C"  float PersonController_RandomizeYPos_m1453813666 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, 0, 5, /*hidden argument*/NULL);
		__this->set_yLevel_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_yLevel_2();
		LayerManager_SetLayer_m2846485351(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_yLevel_2();
		return ((float)((float)(((float)((float)L_3)))-(float)(1.75f)));
	}
}
// System.Single PersonController::RandomizeXPos()
extern "C"  float PersonController_RandomizeXPos_m1090418429 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	float G_B5_0 = 0.0f;
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		__this->set_headingRight_8((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		bool L_1 = __this->get_headingRight_8();
		if (L_1)
		{
			goto IL_003a;
		}
	}
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_2, L_3, /*hidden argument*/NULL);
	}

IL_003a:
	{
		bool L_4 = __this->get_headingRight_8();
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		G_B5_0 = (-4.5f);
		goto IL_0054;
	}

IL_004f:
	{
		G_B5_0 = (4.5f);
	}

IL_0054:
	{
		return G_B5_0;
	}
}
// System.Void PersonController::RandomizeAppearance()
extern "C"  void PersonController_RandomizeAppearance_m3637759145 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_RandomizeAppearance_m3637759145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Outfit_t3147864971 * V_0 = NULL;
	{
		Outfit_t3147864971 * L_0 = (Outfit_t3147864971 *)il2cpp_codegen_object_new(Outfit_t3147864971_il2cpp_TypeInfo_var);
		Outfit__ctor_m2350389386(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral1903744674, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_3 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_2, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_4 = V_0;
		Color_t2020392075  L_5 = L_4->get_shirtColor_2();
		SpriteRenderer_set_color_m2339931967(L_3, L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Transform_Find_m3323476454(L_6, _stringLiteral1005515892, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_8 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_7, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_9 = V_0;
		Color_t2020392075  L_10 = L_9->get_pantsColor_1();
		SpriteRenderer_set_color_m2339931967(L_8, L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Transform_Find_m3323476454(L_11, _stringLiteral1604514370, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_13 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_12, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_14 = V_0;
		Color_t2020392075  L_15 = L_14->get_pantsColor_1();
		SpriteRenderer_set_color_m2339931967(L_13, L_15, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Transform_Find_m3323476454(L_16, _stringLiteral2622853536, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_18 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_17, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_19 = V_0;
		Color_t2020392075  L_20 = L_19->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_18, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Transform_Find_m3323476454(L_21, _stringLiteral2064390248, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_23 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_22, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_24 = V_0;
		Color_t2020392075  L_25 = L_24->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_23, L_25, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = Transform_Find_m3323476454(L_26, _stringLiteral2105580454, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_28 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_27, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_29 = V_0;
		Color_t2020392075  L_30 = L_29->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_28, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::DressIn(Outfit)
extern "C"  void PersonController_DressIn_m2898277795 (PersonController_t256544891 * __this, Outfit_t3147864971 * ___outfit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_DressIn_m2898277795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Transform_Find_m3323476454(L_0, _stringLiteral1903744674, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_2 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_1, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_3 = ___outfit0;
		Color_t2020392075  L_4 = L_3->get_shirtColor_2();
		SpriteRenderer_set_color_m2339931967(L_2, L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Transform_Find_m3323476454(L_5, _stringLiteral1005515892, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_7 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_6, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_8 = ___outfit0;
		Color_t2020392075  L_9 = L_8->get_pantsColor_1();
		SpriteRenderer_set_color_m2339931967(L_7, L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Transform_Find_m3323476454(L_10, _stringLiteral1604514370, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_12 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_11, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_13 = ___outfit0;
		Color_t2020392075  L_14 = L_13->get_pantsColor_1();
		SpriteRenderer_set_color_m2339931967(L_12, L_14, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = Transform_Find_m3323476454(L_15, _stringLiteral2622853536, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_17 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_16, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_18 = ___outfit0;
		Color_t2020392075  L_19 = L_18->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_17, L_19, /*hidden argument*/NULL);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Transform_Find_m3323476454(L_20, _stringLiteral2064390248, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_22 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_21, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_23 = ___outfit0;
		Color_t2020392075  L_24 = L_23->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_22, L_24, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Transform_Find_m3323476454(L_25, _stringLiteral2105580454, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_27 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_26, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Outfit_t3147864971 * L_28 = ___outfit0;
		Color_t2020392075  L_29 = L_28->get_skinColor_0();
		SpriteRenderer_set_color_m2339931967(L_27, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::MakeAcquaintance()
extern "C"  void PersonController_MakeAcquaintance_m2362241201 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_MakeAcquaintance_m2362241201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = __this->get_relationship_3();
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = __this->get_relationship_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_002f;
		}
	}

IL_0018:
	{
		V_0 = _stringLiteral2546340657;
		GameObject_t1756533147 * L_2 = __this->get_notifier_5();
		NotificationController_t1306267903 * L_3 = GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(L_2, /*hidden argument*/GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var);
		String_t* L_4 = V_0;
		NotificationController_ShowNotification_m3734402192(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002f:
	{
		int32_t L_5 = __this->get_relationship_3();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0052;
		}
	}
	{
		V_1 = _stringLiteral3869745644;
		GameObject_t1756533147 * L_6 = __this->get_notifier_5();
		NotificationController_t1306267903 * L_7 = GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(L_6, /*hidden argument*/GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var);
		String_t* L_8 = V_1;
		NotificationController_ShowNotification_m3734402192(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0052:
	{
		__this->set_relationship_3(3);
		return;
	}
}
// System.Void PersonController::MakeFriend()
extern "C"  void PersonController_MakeFriend_m3389549496 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_MakeFriend_m3389549496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = __this->get_relationship_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)5))))
		{
			goto IL_0023;
		}
	}
	{
		V_0 = _stringLiteral1077196766;
		GameObject_t1756533147 * L_1 = __this->get_notifier_5();
		NotificationController_t1306267903 * L_2 = GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(L_1, /*hidden argument*/GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var);
		String_t* L_3 = V_0;
		NotificationController_ShowNotification_m3734402192(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		int32_t L_4 = __this->get_relationship_3();
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0046;
		}
	}
	{
		V_1 = _stringLiteral3517285394;
		GameObject_t1756533147 * L_5 = __this->get_notifier_5();
		NotificationController_t1306267903 * L_6 = GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(L_5, /*hidden argument*/GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var);
		String_t* L_7 = V_1;
		NotificationController_ShowNotification_m3734402192(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0046:
	{
		__this->set_relationship_3(4);
		return;
	}
}
// System.Void PersonController::MakeBFF()
extern "C"  void PersonController_MakeBFF_m2790855184 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_MakeBFF_m2790855184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_relationship_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_0023;
		}
	}
	{
		V_0 = _stringLiteral3291630071;
		GameObject_t1756533147 * L_1 = __this->get_notifier_5();
		NotificationController_t1306267903 * L_2 = GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290(L_1, /*hidden argument*/GameObject_GetComponent_TisNotificationController_t1306267903_m3278002290_MethodInfo_var);
		String_t* L_3 = V_0;
		NotificationController_ShowNotification_m3734402192(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		__this->set_relationship_3(5);
		return;
	}
}
// System.Void PersonController::MakeBully()
extern "C"  void PersonController_MakeBully_m3193116814 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_MakeBully_m3193116814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_relationship_3(1);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Transform_Find_m3323476454(L_0, _stringLiteral1465256952, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_2 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_1, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_3 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		SpriteRenderer_set_color_m2339931967(L_2, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Transform_Find_m3323476454(L_4, _stringLiteral2949419224, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_6 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_5, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_7 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		SpriteRenderer_set_color_m2339931967(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::StartWalking()
extern "C"  void PersonController_StartWalking_m2205466975 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_StartWalking_m2205466975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetBool_m2305662531(L_0, _stringLiteral2962743445, (bool)1, /*hidden argument*/NULL);
		__this->set_currentlyWalking_7((bool)1);
		return;
	}
}
// System.Void PersonController::StopWalking()
extern "C"  void PersonController_StopWalking_m2084113711 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_StopWalking_m2084113711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetBool_m2305662531(L_0, _stringLiteral2962743445, (bool)0, /*hidden argument*/NULL);
		__this->set_currentlyWalking_7((bool)0);
		return;
	}
}
// System.Void PersonController::TurnAround()
extern "C"  void PersonController_TurnAround_m2621683120 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_headingRight_8();
		__this->set_headingRight_8((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		bool L_1 = __this->get_headingRight_8();
		if (L_1)
		{
			goto IL_003e;
		}
	}
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_2, L_3, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_003e:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_4, L_5, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void PersonController::Wave()
extern "C"  void PersonController_Wave_m902357639 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_Wave_m902357639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PersonController_PauseWalking_m2168070195(__this, (1.0f), (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral2237410460, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::FistBump()
extern "C"  void PersonController_FistBump_m3979884468 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_FistBump_m3979884468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PersonController_PauseWalking_m2168070195(__this, (1.0f), (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral851542544, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::Hug()
extern "C"  void PersonController_Hug_m4220064788 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_Hug_m4220064788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PersonController_PauseWalking_m2168070195(__this, (2.25f), (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral1751001121, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::Kick()
extern "C"  void PersonController_Kick_m2572509440 (PersonController_t256544891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_Kick_m2572509440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PersonController_PauseWalking_m2168070195(__this, (1.0f), (bool)1, /*hidden argument*/NULL);
		Animator_t69676727 * L_0 = __this->get_animator_6();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral2898688624, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral1604514370, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_3 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_2, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Renderer_set_sortingOrder_m809829562(L_3, ((int32_t)10), /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Transform_Find_m3323476454(L_4, _stringLiteral2949419224, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_6 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_5, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Renderer_set_sortingOrder_m809829562(L_6, ((int32_t)11), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::PauseRandomTime(UnityEngine.Vector2)
extern "C"  void PersonController_PauseRandomTime_m48163192 (PersonController_t256544891 * __this, Vector2_t2243707579  ___vector0, const MethodInfo* method)
{
	{
		float L_0 = (&___vector0)->get_x_0();
		float L_1 = (&___vector0)->get_y_1();
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		PersonController_PauseWalking_m2168070195(__this, L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersonController::PauseWalking(System.Single,System.Boolean)
extern "C"  void PersonController_PauseWalking_m2168070195 (PersonController_t256544891 * __this, float ___seconds0, bool ___runAway1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonController_PauseWalking_m2168070195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PersonController_StopWalking_m2084113711(__this, /*hidden argument*/NULL);
		bool L_0 = ___runAway1;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		float L_1 = ___seconds0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1329086068, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		float L_2 = ___seconds0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral2054002869, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PicturePersonController::.ctor()
extern "C"  void PicturePersonController__ctor_m55544202 (PicturePersonController_t1772711123 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PicturePersonController::Start()
extern "C"  void PicturePersonController_Start_m2577446190 (PicturePersonController_t1772711123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PicturePersonController_Start_m2577446190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpawnPeople_t2291462312 * L_0 = __this->get_spawner_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		PicturePersonController_RandomizeAppearance_m1140662613(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Conocidos_t996939829_il2cpp_TypeInfo_var);
		Conocidos_Add_m3740580729(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void PicturePersonController::RandomizeAppearance()
extern "C"  void PicturePersonController_RandomizeAppearance_m1140662613 (PicturePersonController_t1772711123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PicturePersonController_RandomizeAppearance_m1140662613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_1 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color__ctor_m1909920690((&V_0), L_0, L_1, L_2, (1.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Transform_Find_m3323476454(L_3, _stringLiteral1903744674, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_5 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_4, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_6 = V_0;
		SpriteRenderer_set_color_m2339931967(L_5, L_6, /*hidden argument*/NULL);
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color__ctor_m1909920690((&V_0), L_7, L_8, L_9, (1.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Transform_Find_m3323476454(L_10, _stringLiteral1005515892, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_12 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_11, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_13 = V_0;
		SpriteRenderer_set_color_m2339931967(L_12, L_13, /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Transform_Find_m3323476454(L_14, _stringLiteral1604514370, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_16 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_15, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_17 = V_0;
		SpriteRenderer_set_color_m2339931967(L_16, L_17, /*hidden argument*/NULL);
		float L_18 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_19 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_20 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color__ctor_m1909920690((&V_0), L_18, L_19, L_20, (1.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Transform_Find_m3323476454(L_21, _stringLiteral2622853536, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_23 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_22, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_24 = V_0;
		SpriteRenderer_set_color_m2339931967(L_23, L_24, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Transform_Find_m3323476454(L_25, _stringLiteral2064390248, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_27 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_26, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_28 = V_0;
		SpriteRenderer_set_color_m2339931967(L_27, L_28, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = Transform_Find_m3323476454(L_29, _stringLiteral2105580454, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_31 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(L_30, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Color_t2020392075  L_32 = V_0;
		SpriteRenderer_set_color_m2339931967(L_31, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PicturePersonController::AddDuringGame()
extern "C"  void PicturePersonController_AddDuringGame_m973809354 (PicturePersonController_t1772711123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PicturePersonController_AddDuringGame_m973809354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_animator_2();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral152376095, /*hidden argument*/NULL);
		PicturePersonController_RandomizeAppearance_m1140662613(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Conocidos_t996939829_il2cpp_TypeInfo_var);
		Conocidos_Add_m3740580729(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		SpawnPeople_t2291462312 * L_2 = __this->get_spawner_3();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Outfit_t3147864971 * L_4 = (Outfit_t3147864971 *)il2cpp_codegen_object_new(Outfit_t3147864971_il2cpp_TypeInfo_var);
		Outfit__ctor_m80864268(L_4, L_3, /*hidden argument*/NULL);
		SpawnPeople_AddFromOutfit_m1509951326(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_4((1.0f));
		__this->set_yStep_5((1.0f));
		__this->set_eyesight_6((2.0f));
		__this->set_timeBetweenMove_14((0.4f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Start()
extern "C"  void PlayerController_Start_m3606284888 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Start_m3606284888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_facingRight_8((bool)1);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		LayerManager_SetLayer_m2846485351(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_game_2();
		ScoreManager_t3573108141 * L_2 = GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996(L_1, /*hidden argument*/GameObject_GetComponent_TisScoreManager_t3573108141_m3215861996_MethodInfo_var);
		__this->set_scoreboard_3(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, 5, _stringLiteral1299152942, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Update()
extern "C"  void PlayerController_Update_m4228472513 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		PlayerController_HandleMovementInput_m3771922523(__this, /*hidden argument*/NULL);
		PlayerController_HandleInteractionInput_m2604908464(__this, /*hidden argument*/NULL);
		PlayerController_UpdateEyeContact_m3487024180(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::HandleMovementInput()
extern "C"  void PlayerController_HandleMovementInput_m3771922523 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_HandleMovementInput_m3771922523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_00b2;
		}
	}
	{
		Animator_t69676727 * L_2 = __this->get_animator_9();
		Animator_SetBool_m2305662531(L_2, _stringLiteral2962743445, (bool)1, /*hidden argument*/NULL);
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_005d;
		}
	}
	{
		__this->set_facingRight_8((bool)0);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_005d:
	{
		__this->set_facingRight_8((bool)1);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0083:
	{
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = Vector2_get_right_m28012078(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_speed_4();
		Vector2_t2243707579  L_13 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Transform_Translate_m3316827744(L_8, L_14, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_00b2:
	{
		Animator_t69676727 * L_15 = __this->get_animator_9();
		Animator_SetBool_m2305662531(L_15, _stringLiteral2962743445, (bool)0, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_16 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		V_1 = L_16;
		float L_17 = V_1;
		if ((!(((float)L_17) > ((float)(0.0f)))))
		{
			goto IL_0134;
		}
	}
	{
		bool L_18 = PlayerController_CanMoveUp_m4049947684(__this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_012f;
		}
	}
	{
		int32_t L_19 = __this->get_yLevel_7();
		__this->set_yLevel_7(((int32_t)((int32_t)L_19+(int32_t)1)));
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_yLevel_7();
		LayerManager_SetLayer_m2846485351(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_23 = __this->get_yStep_5();
		Vector2_t2243707579  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3067419446(&L_24, (0.0f), L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Transform_Translate_m3316827744(L_22, L_25, /*hidden argument*/NULL);
		float L_26 = __this->get_timeBetweenMove_14();
		__this->set_moveUpTimer_15(L_26);
	}

IL_012f:
	{
		goto IL_013f;
	}

IL_0134:
	{
		__this->set_moveUpTimer_15((0.0f));
	}

IL_013f:
	{
		float L_27 = V_1;
		if ((!(((float)L_27) < ((float)(0.0f)))))
		{
			goto IL_01a6;
		}
	}
	{
		bool L_28 = PlayerController_CanMoveDown_m345328991(__this, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_01a1;
		}
	}
	{
		int32_t L_29 = __this->get_yLevel_7();
		__this->set_yLevel_7(((int32_t)((int32_t)L_29-(int32_t)1)));
		GameObject_t1756533147 * L_30 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		int32_t L_31 = __this->get_yLevel_7();
		LayerManager_SetLayer_m2846485351(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_33 = __this->get_yStep_5();
		Vector2_t2243707579  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector2__ctor_m3067419446(&L_34, (0.0f), ((-L_33)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		Transform_Translate_m3316827744(L_32, L_35, /*hidden argument*/NULL);
		float L_36 = __this->get_timeBetweenMove_14();
		__this->set_moveDownTimer_16(L_36);
	}

IL_01a1:
	{
		goto IL_01b1;
	}

IL_01a6:
	{
		__this->set_moveDownTimer_16((0.0f));
	}

IL_01b1:
	{
		float L_37 = __this->get_moveDownTimer_16();
		float L_38 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveDownTimer_16(((float)((float)L_37-(float)L_38)));
		float L_39 = __this->get_moveUpTimer_15();
		float L_40 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveUpTimer_15(((float)((float)L_39-(float)L_40)));
		return;
	}
}
// System.Void PlayerController::HandleInteractionInput()
extern "C"  void PlayerController_HandleInteractionInput_m2604908464 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_HandleInteractionInput_m2604908464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)122), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PlayerController_Wave_m2332726893(__this, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)120), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		PlayerController_FistBump_m3512763140(__this, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)99), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		PlayerController_Hug_m1326409484(__this, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)118), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005b;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1884423134, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void PlayerController::Wave()
extern "C"  void PlayerController_Wave_m2332726893 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Wave_m2332726893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersonController_t256544891 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Animator_t69676727 * L_0 = __this->get_animator_9();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral2237410460, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0113;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_4 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_3, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_0 = L_4;
		PersonController_t256544891 * L_5 = V_0;
		bool L_6 = L_5->get_interactedWith_10();
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return;
	}

IL_0038:
	{
		PersonController_t256544891 * L_7 = V_0;
		int32_t L_8 = L_7->get_relationship_3();
		V_1 = L_8;
		int32_t L_9 = V_1;
		if (((int32_t)((int32_t)L_9-(int32_t)3)) == 0)
		{
			goto IL_0058;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)3)) == 1)
		{
			goto IL_0080;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)3)) == 2)
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00de;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, 3, _stringLiteral3178038520, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_10 = __this->get_scoreboard_3();
		ScoreManager_Award_m2374896584(L_10, 2, /*hidden argument*/NULL);
		PersonController_t256544891 * L_11 = V_0;
		PersonController_Wave_m902357639(L_11, /*hidden argument*/NULL);
		PersonController_t256544891 * L_12 = V_0;
		PersonController_MakeFriend_m3389549496(L_12, /*hidden argument*/NULL);
		goto IL_0107;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-2), _stringLiteral87538262, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_13 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_13, 1, /*hidden argument*/NULL);
		PersonController_t256544891 * L_14 = V_0;
		PersonController_MakeAcquaintance_m2362241201(L_14, /*hidden argument*/NULL);
		PersonController_t256544891 * L_15 = V_0;
		PersonController_PauseWalking_m2168070195(L_15, (0.5f), (bool)0, /*hidden argument*/NULL);
		goto IL_0107;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-3), _stringLiteral924979642, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_16 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_16, 3, /*hidden argument*/NULL);
		PersonController_t256544891 * L_17 = V_0;
		PersonController_MakeAcquaintance_m2362241201(L_17, /*hidden argument*/NULL);
		PersonController_t256544891 * L_18 = V_0;
		PersonController_PauseWalking_m2168070195(L_18, (0.5f), (bool)0, /*hidden argument*/NULL);
		goto IL_0107;
	}

IL_00de:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-2), _stringLiteral2208644768, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_19 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_19, 2, /*hidden argument*/NULL);
		PersonController_t256544891 * L_20 = V_0;
		PersonController_PauseWalking_m2168070195(L_20, (0.5f), (bool)1, /*hidden argument*/NULL);
		goto IL_0107;
	}

IL_0107:
	{
		PersonController_t256544891 * L_21 = V_0;
		L_21->set_interactedWith_10((bool)1);
		goto IL_012b;
	}

IL_0113:
	{
		ScoreManager_t3573108141 * L_22 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_22, 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-3), _stringLiteral2719378512, /*hidden argument*/NULL);
	}

IL_012b:
	{
		return;
	}
}
// System.Void PlayerController::FistBump()
extern "C"  void PlayerController_FistBump_m3512763140 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_FistBump_m3512763140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersonController_t256544891 * V_0 = NULL;
	int32_t V_1 = 0;
	PersonController_t256544891 * V_2 = NULL;
	{
		Animator_t69676727 * L_0 = __this->get_animator_9();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral851542544, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_011d;
		}
	}
	{
		float L_3 = __this->get_eyeContactDist_11();
		if ((!(((float)L_3) < ((float)(1.5f)))))
		{
			goto IL_011d;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_5 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_4, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_0 = L_5;
		PersonController_t256544891 * L_6 = V_0;
		bool L_7 = L_6->get_interactedWith_10();
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		PersonController_t256544891 * L_8 = V_0;
		int32_t L_9 = L_8->get_relationship_3();
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)4)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)5)))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_12 = V_1;
		if (!L_12)
		{
			goto IL_00bf;
		}
	}
	{
		goto IL_00e8;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, 5, _stringLiteral2271763354, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_13 = __this->get_scoreboard_3();
		ScoreManager_Award_m2374896584(L_13, 4, /*hidden argument*/NULL);
		PersonController_t256544891 * L_14 = V_0;
		PersonController_FistBump_m3979884468(L_14, /*hidden argument*/NULL);
		PersonController_t256544891 * L_15 = V_0;
		PersonController_MakeBFF_m2790855184(L_15, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-4), _stringLiteral3628357200, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_16 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_16, 4, /*hidden argument*/NULL);
		PersonController_t256544891 * L_17 = V_0;
		PersonController_MakeFriend_m3389549496(L_17, /*hidden argument*/NULL);
		PersonController_t256544891 * L_18 = V_0;
		PersonController_PauseWalking_m2168070195(L_18, (0.5f), (bool)0, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00bf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-5), _stringLiteral2910639801, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_19 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_19, 8, /*hidden argument*/NULL);
		PersonController_t256544891 * L_20 = V_0;
		PersonController_PauseWalking_m2168070195(L_20, (0.0f), (bool)1, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-3), _stringLiteral959716072, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_21 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_21, 4, /*hidden argument*/NULL);
		PersonController_t256544891 * L_22 = V_0;
		PersonController_PauseWalking_m2168070195(L_22, (0.0f), (bool)1, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0111:
	{
		PersonController_t256544891 * L_23 = V_0;
		L_23->set_interactedWith_10((bool)1);
		goto IL_0181;
	}

IL_011d:
	{
		GameObject_t1756533147 * L_24 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0169;
		}
	}
	{
		GameObject_t1756533147 * L_26 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_27 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_26, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_2 = L_27;
		PersonController_t256544891 * L_28 = V_2;
		PersonController_PauseWalking_m2168070195(L_28, (0.0f), (bool)1, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_29 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_29, 5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-5), _stringLiteral622842013, /*hidden argument*/NULL);
		PersonController_t256544891 * L_30 = V_2;
		L_30->set_interactedWith_10((bool)1);
		goto IL_0181;
	}

IL_0169:
	{
		ScoreManager_t3573108141 * L_31 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_31, 5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-5), _stringLiteral4159693655, /*hidden argument*/NULL);
	}

IL_0181:
	{
		return;
	}
}
// System.Void PlayerController::Hug()
extern "C"  void PlayerController_Hug_m1326409484 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Hug_m1326409484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersonController_t256544891 * V_0 = NULL;
	int32_t V_1 = 0;
	PersonController_t256544891 * V_2 = NULL;
	{
		Animator_t69676727 * L_0 = __this->get_animator_9();
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral1751001121, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00dc;
		}
	}
	{
		float L_3 = __this->get_eyeContactDist_11();
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_00dc;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_5 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_4, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_0 = L_5;
		PersonController_t256544891 * L_6 = V_0;
		bool L_7 = L_6->get_interactedWith_10();
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		PersonController_t256544891 * L_8 = V_0;
		int32_t L_9 = L_8->get_relationship_3();
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)5)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_11 = V_1;
		if (!L_11)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00a7;
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, 6, _stringLiteral1028204982, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_12 = __this->get_scoreboard_3();
		ScoreManager_Award_m2374896584(L_12, 6, /*hidden argument*/NULL);
		PersonController_t256544891 * L_13 = V_0;
		PersonController_Hug_m4220064788(L_13, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-10), _stringLiteral3074348318, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_14 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_14, ((int32_t)10), /*hidden argument*/NULL);
		PersonController_t256544891 * L_15 = V_0;
		PersonController_Kick_m2572509440(L_15, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_00a7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-4), _stringLiteral2821890191, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_16 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_16, 4, /*hidden argument*/NULL);
		PersonController_t256544891 * L_17 = V_0;
		PersonController_PauseWalking_m2168070195(L_17, (0.0f), (bool)1, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_00d0:
	{
		PersonController_t256544891 * L_18 = V_0;
		L_18->set_interactedWith_10((bool)1);
		goto IL_0140;
	}

IL_00dc:
	{
		GameObject_t1756533147 * L_19 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0128;
		}
	}
	{
		GameObject_t1756533147 * L_21 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_22 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_21, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_2 = L_22;
		PersonController_t256544891 * L_23 = V_2;
		PersonController_PauseWalking_m2168070195(L_23, (0.5f), (bool)1, /*hidden argument*/NULL);
		ScoreManager_t3573108141 * L_24 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_24, 5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-8), _stringLiteral2273171932, /*hidden argument*/NULL);
		PersonController_t256544891 * L_25 = V_2;
		L_25->set_interactedWith_10((bool)1);
		goto IL_0140;
	}

IL_0128:
	{
		ScoreManager_t3573108141 * L_26 = __this->get_scoreboard_3();
		ScoreManager_Penalize_m3478917589(L_26, 6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		SocialLog_Add_m2722617324(NULL /*static, unused*/, ((int32_t)-8), _stringLiteral2267285084, /*hidden argument*/NULL);
	}

IL_0140:
	{
		return;
	}
}
// System.Void PlayerController::UpdateEyeContact()
extern "C"  void PlayerController_UpdateEyeContact_m3487024180 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_UpdateEyeContact_m3487024180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	Enumerator_t660383953  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	ShapeModule_t2888832346  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1756533147 * L_0 = __this->get_existingEyeContact_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_existingEyeContact_10();
		PersonController_t256544891 * L_3 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_2, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		bool L_4 = L_3->get_interactedWith_10();
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		__this->set_existingEyeContact_10((GameObject_t1756533147 *)NULL);
	}

IL_002c:
	{
		GameObject_t1756533147 * L_5 = __this->get_existingEyeContact_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_008a;
		}
	}
	{
		GameObject_t1756533147 * L_7 = __this->get_existingEyeContact_10();
		bool L_8 = PlayerController_CanSee_m1619983851(__this, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008a;
		}
	}
	{
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = (&V_0)->get_x_1();
		GameObject_t1756533147 * L_12 = __this->get_existingEyeContact_10();
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = fabsf(((float)((float)L_11-(float)L_15)));
		__this->set_eyeContactDist_11(L_16);
		goto IL_012b;
	}

IL_008a:
	{
		__this->set_eyeContactDist_11((std::numeric_limits<float>::infinity()));
		__this->set_eyeContactWith_12((GameObject_t1756533147 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterManager_t2152395578_il2cpp_TypeInfo_var);
		List_1_t1125654279 * L_17 = ((CharacterManager_t2152395578_StaticFields*)CharacterManager_t2152395578_il2cpp_TypeInfo_var->static_fields)->get_characters_2();
		Enumerator_t660383953  L_18 = List_1_GetEnumerator_m970439620(L_17, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_3 = L_18;
	}

IL_00a7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_010c;
		}

IL_00ac:
		{
			GameObject_t1756533147 * L_19 = Enumerator_get_Current_m2389888842((&V_3), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_2 = L_19;
			GameObject_t1756533147 * L_20 = V_2;
			bool L_21 = PlayerController_CanSee_m1619983851(__this, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_010c;
			}
		}

IL_00c0:
		{
			Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
			Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
			V_5 = L_23;
			float L_24 = (&V_5)->get_x_1();
			GameObject_t1756533147 * L_25 = V_2;
			Transform_t3275118058 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
			Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
			V_6 = L_27;
			float L_28 = (&V_6)->get_x_1();
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
			float L_29 = fabsf(((float)((float)L_24-(float)L_28)));
			V_4 = L_29;
			float L_30 = V_4;
			float L_31 = __this->get_eyeContactDist_11();
			if ((!(((float)L_30) < ((float)L_31))))
			{
				goto IL_010c;
			}
		}

IL_00fd:
		{
			float L_32 = V_4;
			__this->set_eyeContactDist_11(L_32);
			GameObject_t1756533147 * L_33 = V_2;
			__this->set_eyeContactWith_12(L_33);
		}

IL_010c:
		{
			bool L_34 = Enumerator_MoveNext_m3635305532((&V_3), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_34)
			{
				goto IL_00ac;
			}
		}

IL_0118:
		{
			IL2CPP_LEAVE(0x12B, FINALLY_011d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_011d;
	}

FINALLY_011d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_3), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(285)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(285)
	{
		IL2CPP_JUMP_TBL(0x12B, IL_012b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_012b:
	{
		GameObject_t1756533147 * L_35 = __this->get_existingEyeContact_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0171;
		}
	}
	{
		GameObject_t1756533147 * L_37 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0161;
		}
	}
	{
		GameObject_t1756533147 * L_39 = __this->get_existingEyeContact_10();
		GameObject_t1756533147 * L_40 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0171;
		}
	}

IL_0161:
	{
		GameObject_t1756533147 * L_42 = __this->get_existingEyeContact_10();
		PersonController_t256544891 * L_43 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_42, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		PersonController_StartWalking_m2205466975(L_43, /*hidden argument*/NULL);
	}

IL_0171:
	{
		GameObject_t1756533147 * L_44 = __this->get_eyeContactWith_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_45 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_02cf;
		}
	}
	{
		GameObject_t1756533147 * L_46 = __this->get_eyeContactWith_12();
		__this->set_existingEyeContact_10(L_46);
		GameObject_t1756533147 * L_47 = __this->get_eyeContactWith_12();
		PersonController_t256544891 * L_48 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_47, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		PersonController_StopWalking_m2084113711(L_48, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_49 = __this->get_eyeContactParticles_13();
		GameObject_SetActive_m2887581199(L_49, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = Transform_get_position_m1104419803(L_50, /*hidden argument*/NULL);
		V_8 = L_51;
		float L_52 = (&V_8)->get_x_1();
		float L_53 = __this->get_eyeContactDist_11();
		V_7 = ((float)((float)L_52+(float)((float)((float)L_53/(float)(2.0f)))));
		Transform_t3275118058 * L_54 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Transform_get_position_m1104419803(L_54, /*hidden argument*/NULL);
		V_10 = L_55;
		float L_56 = (&V_10)->get_y_2();
		V_9 = ((float)((float)L_56+(float)(0.55f)));
		bool L_57 = __this->get_facingRight_8();
		if (!L_57)
		{
			goto IL_021c;
		}
	}
	{
		GameObject_t1756533147 * L_58 = __this->get_eyeContactParticles_13();
		Transform_t3275118058 * L_59 = GameObject_get_transform_m909382139(L_58, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_60 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_59, L_60, /*hidden argument*/NULL);
		goto IL_0263;
	}

IL_021c:
	{
		Transform_t3275118058 * L_61 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_62 = Transform_get_position_m1104419803(L_61, /*hidden argument*/NULL);
		V_11 = L_62;
		float L_63 = (&V_11)->get_x_1();
		float L_64 = __this->get_eyeContactDist_11();
		V_7 = ((float)((float)L_63-(float)((float)((float)L_64/(float)(2.0f)))));
		GameObject_t1756533147 * L_65 = __this->get_eyeContactParticles_13();
		Transform_t3275118058 * L_66 = GameObject_get_transform_m909382139(L_65, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_67 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (180.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(L_66, L_67, /*hidden argument*/NULL);
	}

IL_0263:
	{
		GameObject_t1756533147 * L_68 = __this->get_eyeContactParticles_13();
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		float L_70 = V_7;
		float L_71 = V_9;
		Vector3_t2243707580  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Vector3__ctor_m2638739322(&L_72, L_70, L_71, (-1.0f), /*hidden argument*/NULL);
		Transform_set_position_m2469242620(L_69, L_72, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_73 = __this->get_eyeContactParticles_13();
		ParticleSystem_t3394631041 * L_74 = GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749(L_73, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749_MethodInfo_var);
		ShapeModule_t2888832346  L_75 = ParticleSystem_get_shape_m1960653537(L_74, /*hidden argument*/NULL);
		V_12 = L_75;
		float L_76 = __this->get_eyeContactDist_11();
		Transform_t3275118058 * L_77 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_78 = Transform_get_localScale_m3074381503(L_77, /*hidden argument*/NULL);
		V_13 = L_78;
		float L_79 = (&V_13)->get_x_1();
		Vector3_t2243707580  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Vector3__ctor_m2638739322(&L_80, ((float)((float)((float)((float)L_76+(float)L_79))-(float)(0.15f))), (0.05f), (1.0f), /*hidden argument*/NULL);
		ShapeModule_set_box_m3050412983((&V_12), L_80, /*hidden argument*/NULL);
		goto IL_02e2;
	}

IL_02cf:
	{
		__this->set_existingEyeContact_10((GameObject_t1756533147 *)NULL);
		GameObject_t1756533147 * L_81 = __this->get_eyeContactParticles_13();
		GameObject_SetActive_m2887581199(L_81, (bool)0, /*hidden argument*/NULL);
	}

IL_02e2:
	{
		return;
	}
}
// System.Boolean PlayerController::CanSee(UnityEngine.GameObject)
extern "C"  bool PlayerController_CanSee_m1619983851 (PlayerController_t4148409433 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_CanSee_m1619983851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersonController_t256544891 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		GameObject_t1756533147 * L_0 = ___obj0;
		PersonController_t256544891 * L_1 = GameObject_GetComponent_TisPersonController_t256544891_m234165472(L_0, /*hidden argument*/GameObject_GetComponent_TisPersonController_t256544891_m234165472_MethodInfo_var);
		V_0 = L_1;
		PersonController_t256544891 * L_2 = V_0;
		bool L_3 = L_2->get_interactedWith_10();
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		PersonController_t256544891 * L_4 = V_0;
		int32_t L_5 = L_4->get_yLevel_2();
		int32_t L_6 = __this->get_yLevel_7();
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0027;
		}
	}
	{
		return (bool)0;
	}

IL_0027:
	{
		PersonController_t256544891 * L_7 = V_0;
		bool L_8 = L_7->get_headingRight_8();
		bool L_9 = __this->get_facingRight_8();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_003a;
		}
	}
	{
		return (bool)0;
	}

IL_003a:
	{
		GameObject_t1756533147 * L_10 = ___obj0;
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_14 = fabsf(L_13);
		if ((!(((float)L_14) > ((float)(4.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}

IL_005e:
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_x_1();
		GameObject_t1756533147 * L_18 = ___obj0;
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Transform_get_position_m1104419803(L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = (&V_4)->get_x_1();
		V_2 = ((float)((float)L_17-(float)L_21));
		bool L_22 = __this->get_facingRight_8();
		if (!L_22)
		{
			goto IL_00b1;
		}
	}
	{
		float L_23 = V_2;
		if ((!(((double)(((double)((double)L_23)))) < ((double)(-0.5)))))
		{
			goto IL_00b1;
		}
	}
	{
		float L_24 = V_2;
		float L_25 = __this->get_eyesight_6();
		if ((!(((float)L_24) >= ((float)((-L_25))))))
		{
			goto IL_00b1;
		}
	}
	{
		return (bool)1;
	}

IL_00b1:
	{
		bool L_26 = __this->get_facingRight_8();
		if (L_26)
		{
			goto IL_00da;
		}
	}
	{
		float L_27 = V_2;
		if ((!(((double)(((double)((double)L_27)))) > ((double)(0.5)))))
		{
			goto IL_00da;
		}
	}
	{
		float L_28 = V_2;
		float L_29 = __this->get_eyesight_6();
		if ((!(((float)L_28) <= ((float)L_29))))
		{
			goto IL_00da;
		}
	}
	{
		return (bool)1;
	}

IL_00da:
	{
		return (bool)0;
	}
}
// System.Boolean PlayerController::CanMoveUp()
extern "C"  bool PlayerController_CanMoveUp_m4049947684 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B3_0 = 0;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		if ((!(((float)L_2) < ((float)(2.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_moveUpTimer_15();
		G_B3_0 = ((((int32_t)((!(((float)L_3) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 0;
	}

IL_0030:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean PlayerController::CanMoveDown()
extern "C"  bool PlayerController_CanMoveDown_m345328991 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B3_0 = 0;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		if ((!(((float)L_2) > ((float)(-1.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_moveDownTimer_16();
		G_B3_0 = ((((int32_t)((!(((float)L_3) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 0;
	}

IL_0030:
	{
		return (bool)G_B3_0;
	}
}
// System.Void RungManager::.ctor()
extern "C"  void RungManager__ctor_m814464896 (RungManager_t3773876531 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RungManager::Start()
extern "C"  void RungManager_Start_m3245805540 (RungManager_t3773876531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RungManager_Start_m3245805540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_rung_2(1);
		int32_t L_0 = __this->get_rung_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral868350994, L_0, /*hidden argument*/NULL);
		RungManager_UpdateGUI_m4113894772(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_rung_2();
		int32_t L_2 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral1237663345, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_3 = __this->get_rung_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1237663345, L_3, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Int32 RungManager::ClimbLadder()
extern "C"  int32_t RungManager_ClimbLadder_m4020101199 (RungManager_t3773876531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RungManager_ClimbLadder_m4020101199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_rung_2();
		__this->set_rung_2(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_rung_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral868350994, L_1, /*hidden argument*/NULL);
		RungManager_UpdateGUI_m4113894772(__this, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_rung_2();
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral1237663345, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_4 = __this->get_rung_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1237663345, L_4, /*hidden argument*/NULL);
	}

IL_0049:
	{
		int32_t L_5 = __this->get_rung_2();
		return L_5;
	}
}
// System.Void RungManager::UpdateGUI()
extern "C"  void RungManager_UpdateGUI_m4113894772 (RungManager_t3773876531 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_rungText_3();
		int32_t* L_1 = __this->get_address_of_rung_2();
		String_t* L_2 = Int32_ToString_m2960866144(L_1, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void ScoreManager::.ctor()
extern "C"  void ScoreManager__ctor_m1636387560 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager::Start()
extern "C"  void ScoreManager_Start_m1676398040 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Start_m1676398040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_score_2(5);
		int32_t L_0 = __this->get_score_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2636872654, L_0, /*hidden argument*/NULL);
		__this->set_nextRungScore_3(((int32_t)10));
		ScoreManager_UpdateGUI_m653588760(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_score_2();
		int32_t L_2 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2541884565, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_3 = __this->get_score_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2541884565, L_3, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void ScoreManager::Penalize(System.Int32)
extern "C"  void ScoreManager_Penalize_m3478917589 (ScoreManager_t3573108141 * __this, int32_t ___points0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Penalize_m3478917589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_score_2();
		int32_t L_1 = ___points0;
		__this->set_score_2(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		int32_t L_2 = __this->get_score_2();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1884423134, /*hidden argument*/NULL);
	}

IL_0024:
	{
		ScoreManager_UpdateGUI_m653588760(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager::Award(System.Int32)
extern "C"  void ScoreManager_Award_m2374896584 (ScoreManager_t3573108141 * __this, int32_t ___points0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Award_m2374896584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_score_2();
		int32_t L_1 = ___points0;
		__this->set_score_2(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		int32_t L_2 = __this->get_score_2();
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2636872654, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_4 = __this->get_score_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2636872654, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_score_2();
		int32_t L_6 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2541884565, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_7 = __this->get_score_2();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2541884565, L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		int32_t L_8 = __this->get_score_2();
		int32_t L_9 = __this->get_nextRungScore_3();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_006f;
		}
	}
	{
		ScoreManager_LevelUp_m2694938423(__this, /*hidden argument*/NULL);
	}

IL_006f:
	{
		ScoreManager_UpdateGUI_m653588760(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager::UpdateGUI()
extern "C"  void ScoreManager_UpdateGUI_m653588760 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_UpdateGUI_m653588760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		int32_t L_0 = __this->get_score_2();
		int32_t L_1 = __this->get_nextRungScore_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, (1.0f), ((float)((float)(((float)((float)L_0)))/(float)((float)((float)(((float)((float)L_1)))*(float)(1.0f))))), /*hidden argument*/NULL);
		float L_3 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = __this->get_scoreBar_4();
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, L_6, (1.0f), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_m2325460848(L_5, L_7, /*hidden argument*/NULL);
		float L_8 = V_0;
		V_1 = ((float)((float)(1.0f)-(float)L_8));
		float L_9 = V_0;
		V_2 = L_9;
		GameObject_t1756533147 * L_10 = __this->get_scoreBar_4();
		Image_t2042527209 * L_11 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_10, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		float L_12 = V_1;
		float L_13 = V_2;
		Color_t2020392075  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m3811852957(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_14);
		return;
	}
}
// System.Void ScoreManager::LevelUp()
extern "C"  void ScoreManager_LevelUp_m2694938423 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	{
		RungManager_t3773876531 * L_0 = __this->get_rungManager_5();
		RungManager_ClimbLadder_m4020101199(L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_nextRungScore_3();
		__this->set_nextRungScore_3(((int32_t)((int32_t)L_1+(int32_t)((int32_t)15))));
		PicturePersonController_t1772711123 * L_2 = __this->get_picturePerson_6();
		PicturePersonController_AddDuringGame_m973809354(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocialLog::.ctor()
extern "C"  void SocialLog__ctor_m3285551350 (SocialLog_t3634626921 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocialLog::Add(System.Int32,System.String)
extern "C"  void SocialLog_Add_m2722617324 (Il2CppObject * __this /* static, unused */, int32_t ___points0, String_t* ___entry1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SocialLog_Add_m2722617324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SocialLog_t3634626921_il2cpp_TypeInfo_var);
		List_1_t3434760161 * L_0 = ((SocialLog_t3634626921_StaticFields*)SocialLog_t3634626921_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		int32_t L_1 = ___points0;
		String_t* L_2 = ___entry1;
		SocialLogEntry_t4065639029 * L_3 = (SocialLogEntry_t4065639029 *)il2cpp_codegen_object_new(SocialLogEntry_t4065639029_il2cpp_TypeInfo_var);
		SocialLogEntry__ctor_m2511064239(L_3, L_1, L_2, /*hidden argument*/NULL);
		List_1_Add_m608678902(L_0, L_3, /*hidden argument*/List_1_Add_m608678902_MethodInfo_var);
		return;
	}
}
// System.Void SocialLog::.cctor()
extern "C"  void SocialLog__cctor_m527127387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SocialLog__cctor_m527127387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3434760161 * L_0 = (List_1_t3434760161 *)il2cpp_codegen_object_new(List_1_t3434760161_il2cpp_TypeInfo_var);
		List_1__ctor_m1518945482(L_0, /*hidden argument*/List_1__ctor_m1518945482_MethodInfo_var);
		((SocialLog_t3634626921_StaticFields*)SocialLog_t3634626921_il2cpp_TypeInfo_var->static_fields)->set_list_0(L_0);
		return;
	}
}
// System.Void SocialLogEntry::.ctor(System.Int32,System.String)
extern "C"  void SocialLogEntry__ctor_m2511064239 (SocialLogEntry_t4065639029 * __this, int32_t ___points0, String_t* ___entry1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___points0;
		__this->set_points_0(L_0);
		String_t* L_1 = ___entry1;
		__this->set_entry_1(L_1);
		return;
	}
}
// System.Int32 SocialLogEntry::GetPoints()
extern "C"  int32_t SocialLogEntry_GetPoints_m353091267 (SocialLogEntry_t4065639029 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_points_0();
		return L_0;
	}
}
// System.String SocialLogEntry::GetEntry()
extern "C"  String_t* SocialLogEntry_GetEntry_m1397986571 (SocialLogEntry_t4065639029 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_entry_1();
		return L_0;
	}
}
// System.String SocialLogEntry::ToString()
extern "C"  String_t* SocialLogEntry_ToString_m3714031703 (SocialLogEntry_t4065639029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SocialLogEntry_ToString_m3714031703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_points_0();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_2 = __this->get_entry_1();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		ArrayElementTypeCheck (L_3, _stringLiteral304499897);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral304499897);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		int32_t L_5 = __this->get_points_0();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		ArrayElementTypeCheck (L_8, _stringLiteral3636943121);
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral3636943121);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_003f:
	{
		ObjectU5BU5D_t3614634134* L_10 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_11 = __this->get_entry_1();
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_10;
		ArrayElementTypeCheck (L_12, _stringLiteral3697230210);
		(L_12)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral3697230210);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		int32_t L_14 = __this->get_points_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_13;
		ArrayElementTypeCheck (L_17, _stringLiteral3636943121);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral3636943121);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Void SpawnPeople::.ctor()
extern "C"  void SpawnPeople__ctor_m611275791 (SpawnPeople_t2291462312 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnPeople::Start()
extern "C"  void SpawnPeople_Start_m1455043971 (SpawnPeople_t2291462312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPeople_Start_m1455043971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpawnPeople_SpawnAcquaintances_m1057452436(__this, /*hidden argument*/NULL);
		float L_0 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (3.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3034765985, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnPeople::SpawnAcquaintances()
extern "C"  void SpawnPeople_SpawnAcquaintances_m1057452436 (SpawnPeople_t2291462312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPeople_SpawnAcquaintances_m1057452436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Outfit_t3147864971 * V_0 = NULL;
	Enumerator_t2051715777  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Conocidos_t996939829_il2cpp_TypeInfo_var);
		List_1_t2516986103 * L_0 = ((Conocidos_t996939829_StaticFields*)Conocidos_t996939829_il2cpp_TypeInfo_var->static_fields)->get_list_0();
		Enumerator_t2051715777  L_1 = List_1_GetEnumerator_m998891817(L_0, /*hidden argument*/List_1_GetEnumerator_m998891817_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_0010:
		{
			Outfit_t3147864971 * L_2 = Enumerator_get_Current_m4263520361((&V_1), /*hidden argument*/Enumerator_get_Current_m4263520361_MethodInfo_var);
			V_0 = L_2;
			Outfit_t3147864971 * L_3 = V_0;
			SpawnPeople_AddFromOutfit_m1509951326(__this, L_3, /*hidden argument*/NULL);
		}

IL_001f:
		{
			bool L_4 = Enumerator_MoveNext_m2115421005((&V_1), /*hidden argument*/Enumerator_MoveNext_m2115421005_MethodInfo_var);
			if (L_4)
			{
				goto IL_0010;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1278152915((&V_1), /*hidden argument*/Enumerator_Dispose_m1278152915_MethodInfo_var);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003e:
	{
		return;
	}
}
// System.Void SpawnPeople::AddFromOutfit(Outfit)
extern "C"  void SpawnPeople_AddFromOutfit_m1509951326 (SpawnPeople_t2291462312 * __this, Outfit_t3147864971 * ___outfit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPeople_AddFromOutfit_m1509951326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_personPrefab_3();
		GameObject_t1756533147 * L_1 = __this->get_container_2();
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_3 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		GameObject_SendMessage_m1177535567(L_4, _stringLiteral2371106403, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = V_0;
		Outfit_t3147864971 * L_6 = ___outfit0;
		GameObject_SendMessage_m2115020133(L_5, _stringLiteral1977266746, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		Vector2_t2243707579  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3067419446(&L_8, (0.0f), (5.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_9);
		GameObject_SendMessage_m2115020133(L_7, _stringLiteral3335134194, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnPeople::SpawnStranger()
extern "C"  void SpawnPeople_SpawnStranger_m1484849674 (SpawnPeople_t2291462312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPeople_SpawnStranger_m1484849674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_personPrefab_3();
		GameObject_t1756533147 * L_1 = __this->get_container_2();
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (2.0f), (4.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3034765985, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YearbookController::.ctor()
extern "C"  void YearbookController__ctor_m2189157005 (YearbookController_t2017796348 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YearbookController::Update()
extern "C"  void YearbookController_Update_m3130861606 (YearbookController_t2017796348 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YearbookController_Update_m3130861606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)122), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3538073363, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
