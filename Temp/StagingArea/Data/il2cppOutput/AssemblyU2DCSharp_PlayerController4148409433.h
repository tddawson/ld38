﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ScoreManager
struct ScoreManager_t3573108141;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PlayerController::game
	GameObject_t1756533147 * ___game_2;
	// ScoreManager PlayerController::scoreboard
	ScoreManager_t3573108141 * ___scoreboard_3;
	// System.Single PlayerController::speed
	float ___speed_4;
	// System.Single PlayerController::yStep
	float ___yStep_5;
	// System.Single PlayerController::eyesight
	float ___eyesight_6;
	// System.Int32 PlayerController::yLevel
	int32_t ___yLevel_7;
	// System.Boolean PlayerController::facingRight
	bool ___facingRight_8;
	// UnityEngine.Animator PlayerController::animator
	Animator_t69676727 * ___animator_9;
	// UnityEngine.GameObject PlayerController::existingEyeContact
	GameObject_t1756533147 * ___existingEyeContact_10;
	// System.Single PlayerController::eyeContactDist
	float ___eyeContactDist_11;
	// UnityEngine.GameObject PlayerController::eyeContactWith
	GameObject_t1756533147 * ___eyeContactWith_12;
	// UnityEngine.GameObject PlayerController::eyeContactParticles
	GameObject_t1756533147 * ___eyeContactParticles_13;
	// System.Single PlayerController::timeBetweenMove
	float ___timeBetweenMove_14;
	// System.Single PlayerController::moveUpTimer
	float ___moveUpTimer_15;
	// System.Single PlayerController::moveDownTimer
	float ___moveDownTimer_16;

public:
	inline static int32_t get_offset_of_game_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___game_2)); }
	inline GameObject_t1756533147 * get_game_2() const { return ___game_2; }
	inline GameObject_t1756533147 ** get_address_of_game_2() { return &___game_2; }
	inline void set_game_2(GameObject_t1756533147 * value)
	{
		___game_2 = value;
		Il2CppCodeGenWriteBarrier(&___game_2, value);
	}

	inline static int32_t get_offset_of_scoreboard_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___scoreboard_3)); }
	inline ScoreManager_t3573108141 * get_scoreboard_3() const { return ___scoreboard_3; }
	inline ScoreManager_t3573108141 ** get_address_of_scoreboard_3() { return &___scoreboard_3; }
	inline void set_scoreboard_3(ScoreManager_t3573108141 * value)
	{
		___scoreboard_3 = value;
		Il2CppCodeGenWriteBarrier(&___scoreboard_3, value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_yStep_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___yStep_5)); }
	inline float get_yStep_5() const { return ___yStep_5; }
	inline float* get_address_of_yStep_5() { return &___yStep_5; }
	inline void set_yStep_5(float value)
	{
		___yStep_5 = value;
	}

	inline static int32_t get_offset_of_eyesight_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___eyesight_6)); }
	inline float get_eyesight_6() const { return ___eyesight_6; }
	inline float* get_address_of_eyesight_6() { return &___eyesight_6; }
	inline void set_eyesight_6(float value)
	{
		___eyesight_6 = value;
	}

	inline static int32_t get_offset_of_yLevel_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___yLevel_7)); }
	inline int32_t get_yLevel_7() const { return ___yLevel_7; }
	inline int32_t* get_address_of_yLevel_7() { return &___yLevel_7; }
	inline void set_yLevel_7(int32_t value)
	{
		___yLevel_7 = value;
	}

	inline static int32_t get_offset_of_facingRight_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___facingRight_8)); }
	inline bool get_facingRight_8() const { return ___facingRight_8; }
	inline bool* get_address_of_facingRight_8() { return &___facingRight_8; }
	inline void set_facingRight_8(bool value)
	{
		___facingRight_8 = value;
	}

	inline static int32_t get_offset_of_animator_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___animator_9)); }
	inline Animator_t69676727 * get_animator_9() const { return ___animator_9; }
	inline Animator_t69676727 ** get_address_of_animator_9() { return &___animator_9; }
	inline void set_animator_9(Animator_t69676727 * value)
	{
		___animator_9 = value;
		Il2CppCodeGenWriteBarrier(&___animator_9, value);
	}

	inline static int32_t get_offset_of_existingEyeContact_10() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___existingEyeContact_10)); }
	inline GameObject_t1756533147 * get_existingEyeContact_10() const { return ___existingEyeContact_10; }
	inline GameObject_t1756533147 ** get_address_of_existingEyeContact_10() { return &___existingEyeContact_10; }
	inline void set_existingEyeContact_10(GameObject_t1756533147 * value)
	{
		___existingEyeContact_10 = value;
		Il2CppCodeGenWriteBarrier(&___existingEyeContact_10, value);
	}

	inline static int32_t get_offset_of_eyeContactDist_11() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___eyeContactDist_11)); }
	inline float get_eyeContactDist_11() const { return ___eyeContactDist_11; }
	inline float* get_address_of_eyeContactDist_11() { return &___eyeContactDist_11; }
	inline void set_eyeContactDist_11(float value)
	{
		___eyeContactDist_11 = value;
	}

	inline static int32_t get_offset_of_eyeContactWith_12() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___eyeContactWith_12)); }
	inline GameObject_t1756533147 * get_eyeContactWith_12() const { return ___eyeContactWith_12; }
	inline GameObject_t1756533147 ** get_address_of_eyeContactWith_12() { return &___eyeContactWith_12; }
	inline void set_eyeContactWith_12(GameObject_t1756533147 * value)
	{
		___eyeContactWith_12 = value;
		Il2CppCodeGenWriteBarrier(&___eyeContactWith_12, value);
	}

	inline static int32_t get_offset_of_eyeContactParticles_13() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___eyeContactParticles_13)); }
	inline GameObject_t1756533147 * get_eyeContactParticles_13() const { return ___eyeContactParticles_13; }
	inline GameObject_t1756533147 ** get_address_of_eyeContactParticles_13() { return &___eyeContactParticles_13; }
	inline void set_eyeContactParticles_13(GameObject_t1756533147 * value)
	{
		___eyeContactParticles_13 = value;
		Il2CppCodeGenWriteBarrier(&___eyeContactParticles_13, value);
	}

	inline static int32_t get_offset_of_timeBetweenMove_14() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___timeBetweenMove_14)); }
	inline float get_timeBetweenMove_14() const { return ___timeBetweenMove_14; }
	inline float* get_address_of_timeBetweenMove_14() { return &___timeBetweenMove_14; }
	inline void set_timeBetweenMove_14(float value)
	{
		___timeBetweenMove_14 = value;
	}

	inline static int32_t get_offset_of_moveUpTimer_15() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___moveUpTimer_15)); }
	inline float get_moveUpTimer_15() const { return ___moveUpTimer_15; }
	inline float* get_address_of_moveUpTimer_15() { return &___moveUpTimer_15; }
	inline void set_moveUpTimer_15(float value)
	{
		___moveUpTimer_15 = value;
	}

	inline static int32_t get_offset_of_moveDownTimer_16() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___moveDownTimer_16)); }
	inline float get_moveDownTimer_16() const { return ___moveDownTimer_16; }
	inline float* get_address_of_moveDownTimer_16() { return &___moveDownTimer_16; }
	inline void set_moveDownTimer_16(float value)
	{
		___moveDownTimer_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
