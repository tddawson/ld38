﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Relationship3804803770.h"

// ScoreManager
struct ScoreManager_t3573108141;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PersonController
struct  PersonController_t256544891  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 PersonController::yLevel
	int32_t ___yLevel_2;
	// Relationship PersonController::relationship
	int32_t ___relationship_3;
	// ScoreManager PersonController::scoreboard
	ScoreManager_t3573108141 * ___scoreboard_4;
	// UnityEngine.GameObject PersonController::notifier
	GameObject_t1756533147 * ___notifier_5;
	// UnityEngine.Animator PersonController::animator
	Animator_t69676727 * ___animator_6;
	// System.Boolean PersonController::currentlyWalking
	bool ___currentlyWalking_7;
	// System.Boolean PersonController::headingRight
	bool ___headingRight_8;
	// System.Single PersonController::speed
	float ___speed_9;
	// System.Boolean PersonController::interactedWith
	bool ___interactedWith_10;

public:
	inline static int32_t get_offset_of_yLevel_2() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___yLevel_2)); }
	inline int32_t get_yLevel_2() const { return ___yLevel_2; }
	inline int32_t* get_address_of_yLevel_2() { return &___yLevel_2; }
	inline void set_yLevel_2(int32_t value)
	{
		___yLevel_2 = value;
	}

	inline static int32_t get_offset_of_relationship_3() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___relationship_3)); }
	inline int32_t get_relationship_3() const { return ___relationship_3; }
	inline int32_t* get_address_of_relationship_3() { return &___relationship_3; }
	inline void set_relationship_3(int32_t value)
	{
		___relationship_3 = value;
	}

	inline static int32_t get_offset_of_scoreboard_4() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___scoreboard_4)); }
	inline ScoreManager_t3573108141 * get_scoreboard_4() const { return ___scoreboard_4; }
	inline ScoreManager_t3573108141 ** get_address_of_scoreboard_4() { return &___scoreboard_4; }
	inline void set_scoreboard_4(ScoreManager_t3573108141 * value)
	{
		___scoreboard_4 = value;
		Il2CppCodeGenWriteBarrier(&___scoreboard_4, value);
	}

	inline static int32_t get_offset_of_notifier_5() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___notifier_5)); }
	inline GameObject_t1756533147 * get_notifier_5() const { return ___notifier_5; }
	inline GameObject_t1756533147 ** get_address_of_notifier_5() { return &___notifier_5; }
	inline void set_notifier_5(GameObject_t1756533147 * value)
	{
		___notifier_5 = value;
		Il2CppCodeGenWriteBarrier(&___notifier_5, value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___animator_6)); }
	inline Animator_t69676727 * get_animator_6() const { return ___animator_6; }
	inline Animator_t69676727 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t69676727 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier(&___animator_6, value);
	}

	inline static int32_t get_offset_of_currentlyWalking_7() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___currentlyWalking_7)); }
	inline bool get_currentlyWalking_7() const { return ___currentlyWalking_7; }
	inline bool* get_address_of_currentlyWalking_7() { return &___currentlyWalking_7; }
	inline void set_currentlyWalking_7(bool value)
	{
		___currentlyWalking_7 = value;
	}

	inline static int32_t get_offset_of_headingRight_8() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___headingRight_8)); }
	inline bool get_headingRight_8() const { return ___headingRight_8; }
	inline bool* get_address_of_headingRight_8() { return &___headingRight_8; }
	inline void set_headingRight_8(bool value)
	{
		___headingRight_8 = value;
	}

	inline static int32_t get_offset_of_speed_9() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___speed_9)); }
	inline float get_speed_9() const { return ___speed_9; }
	inline float* get_address_of_speed_9() { return &___speed_9; }
	inline void set_speed_9(float value)
	{
		___speed_9 = value;
	}

	inline static int32_t get_offset_of_interactedWith_10() { return static_cast<int32_t>(offsetof(PersonController_t256544891, ___interactedWith_10)); }
	inline bool get_interactedWith_10() const { return ___interactedWith_10; }
	inline bool* get_address_of_interactedWith_10() { return &___interactedWith_10; }
	inline void set_interactedWith_10(bool value)
	{
		___interactedWith_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
