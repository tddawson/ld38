﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnPeople
struct  SpawnPeople_t2291462312  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SpawnPeople::container
	GameObject_t1756533147 * ___container_2;
	// UnityEngine.GameObject SpawnPeople::personPrefab
	GameObject_t1756533147 * ___personPrefab_3;

public:
	inline static int32_t get_offset_of_container_2() { return static_cast<int32_t>(offsetof(SpawnPeople_t2291462312, ___container_2)); }
	inline GameObject_t1756533147 * get_container_2() const { return ___container_2; }
	inline GameObject_t1756533147 ** get_address_of_container_2() { return &___container_2; }
	inline void set_container_2(GameObject_t1756533147 * value)
	{
		___container_2 = value;
		Il2CppCodeGenWriteBarrier(&___container_2, value);
	}

	inline static int32_t get_offset_of_personPrefab_3() { return static_cast<int32_t>(offsetof(SpawnPeople_t2291462312, ___personPrefab_3)); }
	inline GameObject_t1756533147 * get_personPrefab_3() const { return ___personPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_personPrefab_3() { return &___personPrefab_3; }
	inline void set_personPrefab_3(GameObject_t1756533147 * value)
	{
		___personPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___personPrefab_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
