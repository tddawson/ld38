﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outfit
struct  Outfit_t3147864971  : public Il2CppObject
{
public:
	// UnityEngine.Color Outfit::skinColor
	Color_t2020392075  ___skinColor_0;
	// UnityEngine.Color Outfit::pantsColor
	Color_t2020392075  ___pantsColor_1;
	// UnityEngine.Color Outfit::shirtColor
	Color_t2020392075  ___shirtColor_2;

public:
	inline static int32_t get_offset_of_skinColor_0() { return static_cast<int32_t>(offsetof(Outfit_t3147864971, ___skinColor_0)); }
	inline Color_t2020392075  get_skinColor_0() const { return ___skinColor_0; }
	inline Color_t2020392075 * get_address_of_skinColor_0() { return &___skinColor_0; }
	inline void set_skinColor_0(Color_t2020392075  value)
	{
		___skinColor_0 = value;
	}

	inline static int32_t get_offset_of_pantsColor_1() { return static_cast<int32_t>(offsetof(Outfit_t3147864971, ___pantsColor_1)); }
	inline Color_t2020392075  get_pantsColor_1() const { return ___pantsColor_1; }
	inline Color_t2020392075 * get_address_of_pantsColor_1() { return &___pantsColor_1; }
	inline void set_pantsColor_1(Color_t2020392075  value)
	{
		___pantsColor_1 = value;
	}

	inline static int32_t get_offset_of_shirtColor_2() { return static_cast<int32_t>(offsetof(Outfit_t3147864971, ___shirtColor_2)); }
	inline Color_t2020392075  get_shirtColor_2() const { return ___shirtColor_2; }
	inline Color_t2020392075 * get_address_of_shirtColor_2() { return &___shirtColor_2; }
	inline void set_shirtColor_2(Color_t2020392075  value)
	{
		___shirtColor_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
