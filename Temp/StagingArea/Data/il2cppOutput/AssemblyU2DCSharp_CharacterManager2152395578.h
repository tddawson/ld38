﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterManager
struct  CharacterManager_t2152395578  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct CharacterManager_t2152395578_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CharacterManager::characters
	List_1_t1125654279 * ___characters_2;

public:
	inline static int32_t get_offset_of_characters_2() { return static_cast<int32_t>(offsetof(CharacterManager_t2152395578_StaticFields, ___characters_2)); }
	inline List_1_t1125654279 * get_characters_2() const { return ___characters_2; }
	inline List_1_t1125654279 ** get_address_of_characters_2() { return &___characters_2; }
	inline void set_characters_2(List_1_t1125654279 * value)
	{
		___characters_2 = value;
		Il2CppCodeGenWriteBarrier(&___characters_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
