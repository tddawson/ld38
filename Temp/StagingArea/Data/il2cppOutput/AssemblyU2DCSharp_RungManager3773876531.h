﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RungManager
struct  RungManager_t3773876531  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 RungManager::rung
	int32_t ___rung_2;
	// UnityEngine.UI.Text RungManager::rungText
	Text_t356221433 * ___rungText_3;

public:
	inline static int32_t get_offset_of_rung_2() { return static_cast<int32_t>(offsetof(RungManager_t3773876531, ___rung_2)); }
	inline int32_t get_rung_2() const { return ___rung_2; }
	inline int32_t* get_address_of_rung_2() { return &___rung_2; }
	inline void set_rung_2(int32_t value)
	{
		___rung_2 = value;
	}

	inline static int32_t get_offset_of_rungText_3() { return static_cast<int32_t>(offsetof(RungManager_t3773876531, ___rungText_3)); }
	inline Text_t356221433 * get_rungText_3() const { return ___rungText_3; }
	inline Text_t356221433 ** get_address_of_rungText_3() { return &___rungText_3; }
	inline void set_rungText_3(Text_t356221433 * value)
	{
		___rungText_3 = value;
		Il2CppCodeGenWriteBarrier(&___rungText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
