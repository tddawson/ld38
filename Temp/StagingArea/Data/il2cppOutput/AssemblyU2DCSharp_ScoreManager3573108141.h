﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// RungManager
struct RungManager_t3773876531;
// PicturePersonController
struct PicturePersonController_t1772711123;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager
struct  ScoreManager_t3573108141  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ScoreManager::score
	int32_t ___score_2;
	// System.Int32 ScoreManager::nextRungScore
	int32_t ___nextRungScore_3;
	// UnityEngine.GameObject ScoreManager::scoreBar
	GameObject_t1756533147 * ___scoreBar_4;
	// RungManager ScoreManager::rungManager
	RungManager_t3773876531 * ___rungManager_5;
	// PicturePersonController ScoreManager::picturePerson
	PicturePersonController_t1772711123 * ___picturePerson_6;
	// UnityEngine.Animator ScoreManager::newAcquaintanceNotificationAnimator
	Animator_t69676727 * ___newAcquaintanceNotificationAnimator_7;

public:
	inline static int32_t get_offset_of_score_2() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___score_2)); }
	inline int32_t get_score_2() const { return ___score_2; }
	inline int32_t* get_address_of_score_2() { return &___score_2; }
	inline void set_score_2(int32_t value)
	{
		___score_2 = value;
	}

	inline static int32_t get_offset_of_nextRungScore_3() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___nextRungScore_3)); }
	inline int32_t get_nextRungScore_3() const { return ___nextRungScore_3; }
	inline int32_t* get_address_of_nextRungScore_3() { return &___nextRungScore_3; }
	inline void set_nextRungScore_3(int32_t value)
	{
		___nextRungScore_3 = value;
	}

	inline static int32_t get_offset_of_scoreBar_4() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___scoreBar_4)); }
	inline GameObject_t1756533147 * get_scoreBar_4() const { return ___scoreBar_4; }
	inline GameObject_t1756533147 ** get_address_of_scoreBar_4() { return &___scoreBar_4; }
	inline void set_scoreBar_4(GameObject_t1756533147 * value)
	{
		___scoreBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___scoreBar_4, value);
	}

	inline static int32_t get_offset_of_rungManager_5() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___rungManager_5)); }
	inline RungManager_t3773876531 * get_rungManager_5() const { return ___rungManager_5; }
	inline RungManager_t3773876531 ** get_address_of_rungManager_5() { return &___rungManager_5; }
	inline void set_rungManager_5(RungManager_t3773876531 * value)
	{
		___rungManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___rungManager_5, value);
	}

	inline static int32_t get_offset_of_picturePerson_6() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___picturePerson_6)); }
	inline PicturePersonController_t1772711123 * get_picturePerson_6() const { return ___picturePerson_6; }
	inline PicturePersonController_t1772711123 ** get_address_of_picturePerson_6() { return &___picturePerson_6; }
	inline void set_picturePerson_6(PicturePersonController_t1772711123 * value)
	{
		___picturePerson_6 = value;
		Il2CppCodeGenWriteBarrier(&___picturePerson_6, value);
	}

	inline static int32_t get_offset_of_newAcquaintanceNotificationAnimator_7() { return static_cast<int32_t>(offsetof(ScoreManager_t3573108141, ___newAcquaintanceNotificationAnimator_7)); }
	inline Animator_t69676727 * get_newAcquaintanceNotificationAnimator_7() const { return ___newAcquaintanceNotificationAnimator_7; }
	inline Animator_t69676727 ** get_address_of_newAcquaintanceNotificationAnimator_7() { return &___newAcquaintanceNotificationAnimator_7; }
	inline void set_newAcquaintanceNotificationAnimator_7(Animator_t69676727 * value)
	{
		___newAcquaintanceNotificationAnimator_7 = value;
		Il2CppCodeGenWriteBarrier(&___newAcquaintanceNotificationAnimator_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
