﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocialLogEntry
struct  SocialLogEntry_t4065639029  : public Il2CppObject
{
public:
	// System.Int32 SocialLogEntry::points
	int32_t ___points_0;
	// System.String SocialLogEntry::entry
	String_t* ___entry_1;

public:
	inline static int32_t get_offset_of_points_0() { return static_cast<int32_t>(offsetof(SocialLogEntry_t4065639029, ___points_0)); }
	inline int32_t get_points_0() const { return ___points_0; }
	inline int32_t* get_address_of_points_0() { return &___points_0; }
	inline void set_points_0(int32_t value)
	{
		___points_0 = value;
	}

	inline static int32_t get_offset_of_entry_1() { return static_cast<int32_t>(offsetof(SocialLogEntry_t4065639029, ___entry_1)); }
	inline String_t* get_entry_1() const { return ___entry_1; }
	inline String_t** get_address_of_entry_1() { return &___entry_1; }
	inline void set_entry_1(String_t* value)
	{
		___entry_1 = value;
		Il2CppCodeGenWriteBarrier(&___entry_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
