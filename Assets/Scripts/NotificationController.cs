﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationController : MonoBehaviour {

	public Animator animator;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowNotification(string msg) {
		animator.SetTrigger ("showNotification");
		Text txt = transform.FindChild("Notification").GetComponent<Text>();
		txt.text = msg;
	}
}
