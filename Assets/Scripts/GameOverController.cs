﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

	public Text dateText;
	public Text obituaryText;

	void Start() {
		UpdateDate ();
		UpdateObituary ();

		// Clear static lists
		CharacterManager.characters.Clear ();
		Conocidos.list.Clear ();
		SocialLog.list.Clear ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Z)) {
			SceneManager.LoadScene ("PreGame");
		}
	}

	void UpdateDate() {
		dateText.text = System.DateTime.Now.ToString ("MM/dd/yyyy");
	}
	
	void UpdateObituary() {
				string str = "Today we mourn the passing of a social life that once reached rung ";
		str += "<color='#008855'>" + PlayerPrefs.GetInt("gameRung").ToString() + "</color> ";
		str += "of the social ladder with nearly ";
		str += "<color='#008855'>" + (PlayerPrefs.GetInt ("gameHighScore") + 1).ToString () + "</color> ";
		str += "social points to its name.";
		str += "\n\n";
		str += "Since nobody was close enough to the social life to write something nice, here's a list of everything it ever did:";
		str += "\n\n";

		foreach (SocialLogEntry logEntry in SocialLog.list) {
			str += logEntry.ToString () + "\n";
		}

		obituaryText.text = str;
	}
}
