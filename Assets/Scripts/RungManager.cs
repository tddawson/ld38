﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RungManager : MonoBehaviour {

	private int rung;
	public Text rungText;

	// Use this for initialization
	void Start () {
		rung = 1;
		PlayerPrefs.SetInt ("gameRung", rung);
		UpdateGUI ();

		if (rung > PlayerPrefs.GetInt("OverallHighestRung")) {
			PlayerPrefs.SetInt ("OverallHighestRung", rung);
		}
	}

	public int ClimbLadder() {
		rung++;
		PlayerPrefs.SetInt ("gameRung", rung);
		UpdateGUI ();

		if (rung > PlayerPrefs.GetInt("OverallHighestRung")) {
			PlayerPrefs.SetInt ("OverallHighestRung", rung);
		}
		return rung;
	}

	private void UpdateGUI() {
		rungText.text = rung.ToString ();
	}
}
