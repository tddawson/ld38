﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialLog {

	static public List<SocialLogEntry> list = new List<SocialLogEntry>();

	public static void Add(int points, string entry) {
		list.Add (new SocialLogEntry (points, entry));
	}
}
