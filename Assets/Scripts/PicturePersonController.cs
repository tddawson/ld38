﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicturePersonController : MonoBehaviour {

	public Animator animator;
	public SpawnPeople spawner;

	// Use this for initialization
	void Start () {
		if (spawner == null) {
			RandomizeAppearance ();
			Conocidos.Add (gameObject);
		}
	}

	void RandomizeAppearance() {
		// Set shirt ("body") color
		Color c = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
		transform.Find ("CharacterRig/Body").GetComponent<SpriteRenderer> ().color = c;

		// Set pants ("legs") color
		c = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
		transform.Find ("CharacterRig/Leg_Back").GetComponent<SpriteRenderer> ().color = c;
		transform.Find ("CharacterRig/Leg_Front").GetComponent<SpriteRenderer> ().color = c;

		// Set skin color
		c = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
		transform.Find ("CharacterRig/Head").GetComponent<SpriteRenderer> ().color = c;
		transform.Find ("CharacterRig/Arm_Front").GetComponent<SpriteRenderer> ().color = c;
		transform.Find ("CharacterRig/Arm_Back").GetComponent<SpriteRenderer> ().color = c;
	}

	public void AddDuringGame() {
		// Show notification
		animator.SetTrigger("showAcquaintanceNotification");

		RandomizeAppearance ();
		Conocidos.Add (gameObject);
		spawner.AddFromOutfit (new Outfit (gameObject));
	}
}
