﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outfit {

	public Color skinColor;
	public Color pantsColor;
	public Color shirtColor;

	// Make outfit from existing person
	public Outfit(GameObject obj) {
		shirtColor = obj.transform.Find ("CharacterRig/Body").GetComponent<SpriteRenderer> ().color;
		skinColor = obj.transform.Find ("CharacterRig/Head").GetComponent<SpriteRenderer> ().color;
		pantsColor = obj.transform.Find ("CharacterRig/Leg_Front").GetComponent<SpriteRenderer> ().color;
	}

	// Create random outfit
	public Outfit() {
		shirtColor = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
		skinColor = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
		pantsColor = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), 1f);
	}
}
