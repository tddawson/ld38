﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public GameObject game;
	public ScoreManager scoreboard;

	public float speed = 1.0f;
	public float yStep = 1.0f;
	public float eyesight = 2f;
	private int yLevel = 0;
	private bool facingRight;
	public Animator animator;

	private GameObject existingEyeContact;
	private float eyeContactDist;
	public GameObject eyeContactWith;
	public GameObject eyeContactParticles;

	public float timeBetweenMove = .4f;
	private float moveUpTimer = 0f;
	private float moveDownTimer = 0f;

	void Start() {
		facingRight = true;
		LayerManager.SetLayer (gameObject, 0);
		scoreboard = game.GetComponent<ScoreManager> ();

		SocialLog.Add (5, "Made an effort.");
	}
	
	// Update is called once per frame
	void Update () {
		HandleMovementInput ();
		HandleInteractionInput ();
		UpdateEyeContact ();
	}

	void HandleMovementInput() {
		// Horizontal movement
		float hMovement = Input.GetAxis ("Horizontal"); 
		if (hMovement != 0) {
			animator.SetBool ("walking", true);
			if (hMovement < 0) {
				facingRight = false;
				transform.rotation = Quaternion.Euler (0, 180, 0);
			} else {
				facingRight = true;
				transform.rotation = Quaternion.Euler (0, 0, 0);
			}
			transform.Translate (Vector2.right * Time.deltaTime * speed);
		} else {
			animator.SetBool ("walking", false);
		}

		// Vertical movement
		float vMovement = Input.GetAxis ("Vertical");
		if (vMovement > 0) {
			if (CanMoveUp ()) {
				
				yLevel++;
				LayerManager.SetLayer (gameObject, yLevel);

				transform.Translate (new Vector2 (0, yStep));
				moveUpTimer = timeBetweenMove;
			}
		} else {
			moveUpTimer = 0;
		}

		if (vMovement < 0) {
			if (CanMoveDown ()) {
				yLevel--;
				LayerManager.SetLayer (gameObject, yLevel);

				transform.Translate (new Vector2 (0, -yStep));
				moveDownTimer = timeBetweenMove;
			}
		} else {
			moveDownTimer = 0;
		}
		moveDownTimer -= Time.deltaTime;
		moveUpTimer -= Time.deltaTime;
	}

	void HandleInteractionInput() {
		if (Input.GetKeyDown (KeyCode.Z)) {
			Wave ();
		} else if (Input.GetKeyDown (KeyCode.X)) {
			FistBump ();
		} else if (Input.GetKeyDown (KeyCode.C)) {
			Hug ();
		}else if (Input.GetKeyDown (KeyCode.V)) {
			SceneManager.LoadScene ("GameOver");
		}
	}

	void Wave() {
		animator.SetTrigger ("waving");
		if (eyeContactWith) {
			PersonController person = eyeContactWith.GetComponent<PersonController> ();

			if (person.interactedWith) {
				// If you've already interacted with them, ignore.
				return;
			}
			switch (person.relationship) 
			{
				case Relationship.Acquaintance:
					SocialLog.Add (3, "Made a friend by waving to an acquaintance.");
					scoreboard.Award (2);
					person.Wave ();
					person.MakeFriend ();
					break;

				case Relationship.Friend:
					SocialLog.Add (-2, "Turned a friend into an acquaintance by waving.");
					scoreboard.Penalize (1);
					person.MakeAcquaintance ();
					person.PauseWalking (0.5f);
					break;

				case Relationship.BFF:
					SocialLog.Add (-3, "Turned a BFF into an acquaintance by waving.");
					scoreboard.Penalize (3);
					person.MakeAcquaintance ();
					person.PauseWalking (0.5f);
					break;

				default: 
					SocialLog.Add (-2, "Waved at a stranger. How weird.");
					scoreboard.Penalize (2);
					person.PauseWalking (0.5f, true);
					break;
			}
			person.interactedWith = true;
		} else {
			// Waving to nobody like a fool
			scoreboard.Penalize (3);
			SocialLog.Add (-3, "Waved at nobody like a darn fool.");
		}
	}

	void FistBump() {
		animator.SetTrigger ("fistBumping");
		if (eyeContactWith && eyeContactDist < 1.5f) {
			PersonController person = eyeContactWith.GetComponent<PersonController> ();

			if (person.interactedWith) {
				// If you've already interacted with them, ignore.
				return;
			}
			switch (person.relationship) {
				case Relationship.Friend:
					SocialLog.Add (5, "Made a BFF by fist-bumping a friend!");
					scoreboard.Award (4);
					person.FistBump ();	
					person.MakeBFF ();
					break;

				case Relationship.BFF:
					SocialLog.Add (-4, "Turned a BFF into a friend by fist-bumping.");
					scoreboard.Penalize (4);
					person.MakeFriend ();
					person.PauseWalking (0.5f);
					break;

				case Relationship.Stranger:
					SocialLog.Add (-5, "Fist-bumped a stranger who thought they were being attacked.");
					scoreboard.Penalize (8);
					person.PauseWalking (0f, true);
					break;

				default: 
					SocialLog.Add (-3, "Fist-bumped an acquaintance.");
					scoreboard.Penalize (4);
					person.PauseWalking (0f, true);
					break;
			}

			person.interactedWith = true;
		} else if (eyeContactWith) {
			// You just stared someone down and then punched the air. They're going to walk away.
			PersonController person = eyeContactWith.GetComponent<PersonController> ();
			person.PauseWalking (0f, true);

			scoreboard.Penalize (5);
			SocialLog.Add (-5, "Tried fist-bumping from WAY too far away.");

			person.interactedWith = true;
		} else {
			// Fist-bumping the air looks odd.
			scoreboard.Penalize (5);
			SocialLog.Add (-5, "Fist-bumped nobody in particular");
		}
	}

	void Hug() {
		animator.SetTrigger ("hugging");
		if (eyeContactWith && eyeContactDist < 1f) {
			PersonController person = eyeContactWith.GetComponent<PersonController> ();

			if (person.interactedWith) {
				// If you've already interacted with them, ignore.
				return;
			}
			switch (person.relationship) {
				case Relationship.BFF:
					SocialLog.Add (6, "Hugged a BFF!");
					scoreboard.Award (6);
					person.Hug ();
					break;

				case Relationship.Stranger:
					SocialLog.Add (-10, "Hugged a stranger, who thought they were being assaulted.");
					scoreboard.Penalize (10);
					person.Kick ();
					break;

				default: 
					SocialLog.Add (-4, "Hugged someone too early in the relationship.");
					scoreboard.Penalize (4);
					person.PauseWalking (0f, true);
					break;
			}

			person.interactedWith = true;
		} else if (eyeContactWith) {
			// You just stared someone down and then hugged the air. They're going to walk away.
			PersonController person = eyeContactWith.GetComponent<PersonController> ();
			person.PauseWalking (.5f, true);

			scoreboard.Penalize (5);
			SocialLog.Add (-8, "Stared someone down, then hugged the air in front of them.");

			person.interactedWith = true;
		} else {
			// Hugging the air is a weird thing to do.
			scoreboard.Penalize (6);
			SocialLog.Add (-8, "Hugged the invisible man.");
		}
	}

	void UpdateEyeContact() {
		if (existingEyeContact && existingEyeContact.GetComponent<PersonController> ().interactedWith) {
			existingEyeContact = null;
		}
		if (existingEyeContact != null && CanSee(existingEyeContact)) {
			// If we maintain existing eye contact, update and use it
			eyeContactDist = Mathf.Abs (transform.position.x - existingEyeContact.transform.position.x);
		} else {
			// Otherwise, find eye contact, if any.
			eyeContactDist = float.PositiveInfinity;
			eyeContactWith = null;
			foreach (GameObject obj in CharacterManager.characters) {
				if (CanSee (obj)) {
					float dist = Mathf.Abs (transform.position.x - obj.transform.position.x);
					if (dist < eyeContactDist) {
						eyeContactDist = dist;
						eyeContactWith = obj;
					}
				}
			}
		}

		// Free previous person under spell of eye contact, if any.
		if (existingEyeContact && (!eyeContactWith || existingEyeContact != eyeContactWith)) {
			existingEyeContact.GetComponent<PersonController> ().StartWalking ();
		}

		if (eyeContactWith) {
			// Stop person dead in their tracks
			existingEyeContact = eyeContactWith;
			eyeContactWith.GetComponent<PersonController> ().StopWalking ();

			eyeContactParticles.SetActive (true);
			float x = transform.position.x + eyeContactDist / 2;
			float y = transform.position.y + .55f;

			if (facingRight) {
				eyeContactParticles.transform.rotation = Quaternion.Euler (0, 0, 0);
			} else {
				x = transform.position.x - eyeContactDist / 2;
				eyeContactParticles.transform.rotation = Quaternion.Euler (0, 0, 180);
			}
			eyeContactParticles.transform.position = new Vector3 (x, y, -1f);

			var shape = eyeContactParticles.GetComponent<ParticleSystem> ().shape;
			shape.box = new Vector3 (eyeContactDist + transform.localScale.x - .15f, .05f, 1f);
		} else {
			existingEyeContact = null;
			eyeContactParticles.SetActive (false);
		}
	}

	bool CanSee(GameObject obj) {
		PersonController character = obj.GetComponent<PersonController> ();

		// Already interacted with this person on this run
		if (character.interactedWith)
			return false;

		// On different y planes
		if (character.yLevel != yLevel)
			return false;

		// Facing opposite directions
		if (character.headingRight == facingRight)
			return false;

		// Off-screen
		if (Mathf.Abs (obj.transform.position.x) > 4.0f)
			return false;

		float dist = transform.position.x - obj.transform.position.x;
		if (facingRight && dist < -.5 && dist >= -eyesight)
			return true;

		if (!facingRight && dist > .5 && dist <= eyesight)
			return true;

		return false;
	}

	bool CanMoveUp() {
		return transform.position.y < 2 && moveUpTimer <= 0;
	}

	bool CanMoveDown() {
		return transform.position.y > -1 && moveDownTimer <= 0;
	}

}
