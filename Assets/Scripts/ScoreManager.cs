﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

	private int score;
	private int nextRungScore;
	public GameObject scoreBar;
	public RungManager rungManager;
	public PicturePersonController picturePerson;
	public Animator newAcquaintanceNotificationAnimator;

	// Use this for initialization
	void Start () {
		score = 5;
		PlayerPrefs.SetInt ("gameHighScore", score);
		nextRungScore = 10;
		UpdateGUI ();

		if (score > PlayerPrefs.GetInt("OverallHighestLife")) {
			PlayerPrefs.SetInt ("OverallHighestLife", score);
		}
	}

	public void Penalize(int points) {
		score -= points;
		if (score <= 0) {
			SceneManager.LoadScene ("GameOver");
		}
		UpdateGUI ();
	}

	public void Award(int points) {
		score += points;
		if (score > PlayerPrefs.GetInt ("gameHighScore")) {
			PlayerPrefs.SetInt ("gameHighScore", score);

			if (score > PlayerPrefs.GetInt("OverallHighestLife")) {
				PlayerPrefs.SetInt ("OverallHighestLife", score);
			}
		}
		if (score >= nextRungScore) {
			LevelUp ();
		}
		UpdateGUI ();
	}

	private void UpdateGUI() {
		// Update size
		float x = Mathf.Max (0, Mathf.Min (1, score / (nextRungScore * 1.0f)));
		scoreBar.transform.localScale = new Vector3 (x, 1f, 1f);

		// Update color
		float r = 1.0f - x;
		float g = x;
		scoreBar.GetComponent<Image> ().color = new Color (r, g, 0f);
	}

	private void LevelUp() {
		rungManager.ClimbLadder ();
		nextRungScore += 15; // Should increase more based off of the current level.
		picturePerson.AddDuringGame();
//		newAcquaintanceAnimator.SetTrigger("show");
	}
}
