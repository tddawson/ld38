﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialLogEntry {

	private int points;
	private string entry;

	public SocialLogEntry(int points, string entry)
	{
		this.points = points;
		this.entry = entry;
	}

	public int GetPoints() {
		return points;
	}

	public string GetEntry() {
		return entry;
	}

	public string ToString() {
		if (points < 0) {
			return entry + " (<color='#cc0000'>" + points + "</color>)";
		}
		return entry + " (<color='#008855'>+" + points + "</color>)";
	}
}
