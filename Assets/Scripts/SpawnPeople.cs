﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPeople : MonoBehaviour {

	public GameObject container;
	public GameObject personPrefab;

	// Use this for initialization
	void Start () {
		// Draw all acquaintances from yearbook
		SpawnAcquaintances ();

		// Start with one stranger after a few seconds
		Invoke ("SpawnStranger", Random.Range(0f, 3f));
	}

	/**
	 * Create game objects of the people you know.
	 */ 
	void SpawnAcquaintances() {
		foreach (Outfit outfit in Conocidos.list) {
			AddFromOutfit (outfit);
		}
	}

	public void AddFromOutfit(Outfit outfit) {
		GameObject p = (GameObject)Instantiate (personPrefab, container.transform);
		p.SendMessage ("MakeAcquaintance");
		p.SendMessage ("DressIn", outfit);
		p.SendMessage ("PauseRandomTime", new Vector2(0f, 5f));
	}

	/**
	 * Spawn a stranger once every 2-4 seconds
	 */
	void SpawnStranger() {
		Instantiate (personPrefab, container.transform);
		Invoke ("SpawnStranger", Random.Range(2f, 4f));
	}
}
