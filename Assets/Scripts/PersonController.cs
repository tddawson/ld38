﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Relationship {
	Stranger,
	Bully,
	Recognizable,
	Acquaintance,
	Friend,
	BFF
};

public class PersonController : MonoBehaviour {

	public int yLevel;
	public Relationship relationship = Relationship.Stranger;
	private ScoreManager scoreboard;
	public GameObject notifier;
	public Animator animator;
	private bool currentlyWalking;
	public bool headingRight;
	public float speed = .5f;
	public bool interactedWith; // Has player interacted with this person during this walk?

	// Use this for initialization
	void Start () {
		interactedWith = false;
		transform.position = new Vector2 (RandomizeXPos(), RandomizeYPos());
		scoreboard = GameObject.Find("World").GetComponent<ScoreManager> ();

		notifier = GameObject.Find ("Canvas/NotificationPanel");

		// Strangers should start walking right away
		if (relationship == Relationship.Stranger) {
			StartWalking ();
			RandomizeAppearance ();
		}

		CharacterManager.characters.Add (gameObject);
	}

	// Update is called once per frame
	void Update () {
		if (currentlyWalking) {
			transform.Translate (Vector2.right * Time.deltaTime * speed);

			if (WalkedOffScreen()) {
				if (relationship == Relationship.Stranger) {
					Destroy (gameObject);
					CharacterManager.characters.Remove (gameObject);
				} else {
					if (!interactedWith) {
						if (relationship == Relationship.Acquaintance) {
							SocialLog.Add (-1, "Let an acquaintance pass without interacting.");
							scoreboard.Penalize (1);
						} else if (relationship == Relationship.Friend) {
							SocialLog.Add (-1, "Let a friend pass without interacting.");
							scoreboard.Penalize (2);
						} else if (relationship == Relationship.BFF) {
							SocialLog.Add (-3, "Let a BFF pass without interacting.");
							scoreboard.Penalize (3);

							// Make bully?
						}

					}

					StopWalking ();
					interactedWith = false;

					// Flip character so they come from somewhere on this side next time
					TurnAround();
					transform.position = new Vector2(transform.position.x, RandomizeYPos ());
					Invoke("StartWalking", Random.Range(0f, 5f));
				}
			}
		}
	}

	bool WalkedOffScreen() {
		return (headingRight && transform.position.x > 5) || (!headingRight && transform.position.x < -5);
	}

	/**
	 * INITIALIZATION FUNCTIONS
	 */
	float RandomizeYPos() {
		yLevel = Random.Range(0, 5);
		LayerManager.SetLayer (gameObject, yLevel);
		return yLevel - 1.75f;
	}

	float RandomizeXPos() {
		headingRight = Random.Range (0, 2) == 0;
		if (!headingRight) {
			transform.rotation = Quaternion.Euler (0, 180, 0);
		}
		return headingRight ? -4.5f : 4.5f;
	}

	void RandomizeAppearance() {
		// Create a random outfit
		Outfit outfit = new Outfit();

		// Set shirt ("body") color
		transform.Find ("CharacterRig/Body").GetComponent<SpriteRenderer> ().color = outfit.shirtColor;

		// Set pants ("legs") color
		transform.Find ("CharacterRig/Leg_Back").GetComponent<SpriteRenderer> ().color = outfit.pantsColor;
		transform.Find ("CharacterRig/Leg_Front").GetComponent<SpriteRenderer> ().color = outfit.pantsColor;

		// Set skin color
		transform.Find ("CharacterRig/Head").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
		transform.Find ("CharacterRig/Arm_Front").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
		transform.Find ("CharacterRig/Arm_Back").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
	}

	void DressIn(Outfit outfit) {
		// Set shirt ("body") color
		transform.Find ("CharacterRig/Body").GetComponent<SpriteRenderer> ().color = outfit.shirtColor;

		// Set pants ("legs") color
		transform.Find ("CharacterRig/Leg_Back").GetComponent<SpriteRenderer> ().color = outfit.pantsColor;
		transform.Find ("CharacterRig/Leg_Front").GetComponent<SpriteRenderer> ().color = outfit.pantsColor;

		// Set skin color
		transform.Find ("CharacterRig/Head").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
		transform.Find ("CharacterRig/Arm_Front").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
		transform.Find ("CharacterRig/Arm_Back").GetComponent<SpriteRenderer> ().color = outfit.skinColor;
	}

//	public void MakeRecognition() {
//		// If a demotion, display message
//		if (relationship == Relationship.Acquaintance || relationship == Relationship.Friend || relationship == Relationship.BFF) {
//
//		}
//			
//
//		relationship = Relationship.Recognizable;
//
//		// For easy testing
//		transform.Find ("CharacterRig/Head/Hair_1").GetComponent<SpriteRenderer> ().color = Color.white;
//	}

	public void MakeAcquaintance() {
		// If a demotion, display message
		if (relationship == Relationship.Friend || relationship == Relationship.BFF) {
			string msg = "Only a wave?! Demoted to <color=#cc0000>acquaintances</color>.\n" +
				"Wave to acquaintances with <color=#cc0000>[z]</color>.";
			notifier.GetComponent<NotificationController> ().ShowNotification (msg);
		}

		// If a promotion, display message
		if (relationship == Relationship.Recognizable) {
			string msg = "A new <color=#008855>acquaintance</color>!\n" +
				"Next time, wave with <color=#008855>[z]</color>";
			notifier.GetComponent<NotificationController> ().ShowNotification (msg);
		}

		relationship = Relationship.Acquaintance;

		// For easy testing
//		transform.Find ("CharacterRig/Head/Hair_1").GetComponent<SpriteRenderer> ().color = Color.white;
	}

	public void MakeFriend() {
		// If a demotion, display message
		if (relationship == Relationship.BFF) {
			string msg = "Only a fist-bump?! Demoted to <color=#cc0000>friends</color>.\n" +
			             "Fist-bump friends with <color=#cc0000>[x]</color>.";
			notifier.GetComponent<NotificationController> ().ShowNotification (msg);
		}

		// If a promotion, display message
		if (relationship == Relationship.Acquaintance) {
			string msg = "Nice! You're now <color=#008855>friends</color>!\n" +
				"Next time, fist-bump with <color=#008855>[x]</color>";
			notifier.GetComponent<NotificationController> ().ShowNotification (msg);
		}

		relationship = Relationship.Friend;

		// For easy testing
//		transform.Find ("CharacterRig/Head/Hair_1").GetComponent<SpriteRenderer> ().color = Color.blue;
	}

	public void MakeBFF() {
		// If a promotion, display message
		if (relationship == Relationship.Friend) {
			string msg = "A new <color=#008855>BFF</color>!\n" +
				"Next time, hug with <color=#008855>[c]</color>";
			notifier.GetComponent<NotificationController> ().ShowNotification (msg);
		}

		relationship = Relationship.BFF;

		// For easy testing
//		transform.Find ("CharacterRig/Head/Hair_1").GetComponent<SpriteRenderer> ().color = Color.red;
	}

	public void MakeBully() {
		relationship = Relationship.Bully;

		// Make shoes black
		transform.Find ("CharacterRig/Leg_Back/Shoe_Back").GetComponent<SpriteRenderer> ().color = Color.black;
		transform.Find ("CharacterRig/Leg_Front/Shoe_Front").GetComponent<SpriteRenderer> ().color = Color.black;
	}

	public void StartWalking() {
		animator.SetBool ("walking", true);
		currentlyWalking = true;
	}

	public void StopWalking() {
		animator.SetBool ("walking", false);
		currentlyWalking = false;
	}

	public void TurnAround() {
		headingRight = !headingRight;
		if (!headingRight) {
			transform.rotation = Quaternion.Euler (0, 180, 0);
		} else {
			transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}

	public void Wave() {
		PauseWalking ();
		animator.SetTrigger ("waving");
	}

	public void FistBump() {
		PauseWalking ();
		animator.SetTrigger ("fistBumping");
	}

	public void Hug() {
		PauseWalking (2.25f);
		animator.SetTrigger ("hugging");
	}

	public void Kick() {
		PauseWalking (1f, true);
		animator.SetTrigger ("kicking");

		// Make him kick him in the groin
		transform.Find ("CharacterRig/Leg_Front").GetComponent<SpriteRenderer> ().sortingOrder = 10;
		transform.Find ("CharacterRig/Leg_Front/Shoe_Front").GetComponent<SpriteRenderer> ().sortingOrder = 11;
	}

	public void PauseRandomTime(Vector2 vector) {
		PauseWalking (Random.Range (vector.x, vector.y));
	}

	public void PauseWalking(float seconds=1f, bool runAway=false) {
		StopWalking ();
		if (runAway) {
			Invoke ("TurnAround", seconds);
		}
		Invoke ("StartWalking", seconds);
	}
}
