﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerManager : MonoBehaviour {

	// Set the layer of provided person, and sorting layer of all of its children/grandchildren
	public static void SetLayer(GameObject person, int layer) {
		person.layer = LayerMask.NameToLayer("y" + layer);

		Transform rigTransform = person.transform.Find ("CharacterRig").GetComponent<Transform> ();
		foreach (Transform t in rigTransform) {
			t.GetComponent<SpriteRenderer> ().sortingLayerName = "sl_y" + layer;

			// Set for grandchildren. Should be as deep as we need for this game.
			foreach (Transform t2 in t) {
				t2.GetComponent<SpriteRenderer> ().sortingLayerName = "sl_y" + layer;
			}
		}
	}
}
