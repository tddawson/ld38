﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Text txt = gameObject.GetComponent<Text> ();
		txt.text = "Highest Social Life: <color=#008855>" + PlayerPrefs.GetInt("OverallHighestLife", 0) + "</color>\n" +
			"Highest Rung: <color=#008855>" + PlayerPrefs.GetInt("OverallHighestRung", 0) + "</color>";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
